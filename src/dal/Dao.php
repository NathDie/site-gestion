<?php

namespace Nathan\dal;

use PDO;

class Dao
{
    private $driver = "mysql";
    private $host = "localhost";
    private $port = "3306";
    private $dbname = "bibliotheque";
    private $charset = "utf8";
    private $user = "rooot";
    private $password = "1234";
    private $dsn;
    private $dbh;
    public function get_dbh()
    {
        return $this->dbh;
    }

    public function __construct()
    {
        $this->dsn =
            "{$this->driver}:" .
            "host={$this->host};" .
            "port={$this->port};" .
            "dbname={$this->dbname};" .
            "charset={$this->charset};";
    }

    public function open()
    {
        $this->dbh = new PDO($this->dsn, $this->user, $this->password);
    }

    public function close()
    {
        unset($this->dbh);
    }
}
