-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Client :  localhost:3306
-- Généré le :  Lun 11 Mai 2020 à 16:08
-- Version du serveur :  5.7.30-0ubuntu0.18.04.1
-- Version de PHP :  7.4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `bibliotheque`
--

-- --------------------------------------------------------

--
-- Structure de la table `auteur`
--

CREATE TABLE `auteur` (
  `id` int(3) NOT NULL,
  `nom` varchar(100) DEFAULT NULL,
  `biographie` varchar(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `auteur`
--

INSERT INTO `auteur` (`id`, `nom`, `biographie`) VALUES
(1, 'Moira Young', '...'),
(2, 'Goscinny & Uderzo', '...'),
(3, 'Léonora Miano', '...'),
(4, 'George Orwell', '...'),
(5, 'Camille Laurens', '...'),
(6, 'Roland Barthes', '...'),
(7, 'Jean de la Fontaine', '...'),
(8, 'Isaac Asimov', '...'),
(9, 'Emile Zola', '...'),
(10, 'Pierre-Augustin Caron de Beaumarchais', '...'),
(11, 'John Steinbeck', '...'),
(12, 'Molière', '...'),
(13, 'Jean Racine', '...'),
(14, 'Gaëlle Le Guern-Camara', '...'),
(15, 'Laurent Gounelle', '...'),
(16, 'Aurélie Valogne', '...'),
(17, 'Homère', '...'),
(18, 'ChateauBriand', '...'),
(19, 'Daniel Pennac', '...'),
(20, 'Jean Molla', '...'),
(21, 'Jean-Phillipe Arrou-Vignod', '...'),
(22, 'Conan Doyle', '...'),
(23, 'Anne-Sophie & Marie-Aldine Girard', '...'),
(24, 'Danielle Steel', '...'),
(25, 'Mary Higgins Clark', '...'),
(26, 'Chrétien de Troyes', '...'),
(27, 'Robert Silverberg', '...'),
(28, 'Guy de Maupassant', '...'),
(29, 'Mary Shelley', '...'),
(30, 'Marc Levy', '...'),
(31, 'Victor Hugo', '...'),
(32, 'Jay Bennett', '...'),
(33, 'Fred Vargas', '...'),
(34, 'Jean-Michel Lecocq', '...'),
(35, 'Alexandra Lange', '...'),
(36, 'George Harrar', '...'),
(37, 'Sandra Labastie', '...'),
(38, 'Marc Galieu', '...'),
(39, 'Alexandre Bodart Pinto', '...'),
(40, 'Martie-Aude Murail', '...'),
(41, 'Nicci French', '...'),
(42, 'Nathalie Bernard', '...'),
(43, 'Thierry Cohen', '...'),
(44, 'Guillaume Musso', '...'),
(45, 'Francoise Bourdin', '...'),
(46, 'Tatiana de Rosnay', '...'),
(47, 'Louis Pergaud', '...'),
(48, 'Margin Higgins Clark', '...'),
(49, 'Harlan Coben', '...'),
(50, 'Stephenie Meyer', '...'),
(51, 'Gustave Flaubert', '...'),
(52, 'Alfred Jarry', '...'),
(53, 'Eugène Ionesco', '...'),
(54, 'Dai Sijie', '...'),
(55, 'Arthur Rimbaud', '...'),
(56, 'Alphonse Daudet', '...'),
(57, 'Maxence Van Der Meersch', '...'),
(58, 'Jules Vallès', '...'),
(59, 'Jean Anouilh', '...'),
(60, 'Kafka', '...'),
(61, 'Amélie Nothomb', '...'),
(62, 'Jean-Jacques Rousseau', '...'),
(63, 'Francoise Mauriac', '...'),
(64, 'Martine Provis', '...'),
(65, 'Jack London', '...'),
(66, 'Jean Ginio', '...'),
(67, 'Guillaume Appolinaire', '...'),
(68, 'Mary Jane Clark', '...'),
(69, 'Patrick Lapeyre', '...'),
(70, 'Helena S. Paige', '...'),
(71, 'Sally Mandel', '...'),
(72, 'Torsten Pettersson', '...'),
(73, 'Liane Moriarty', '...'),
(74, 'Katherine Scholes', '...'),
(75, 'Douglas Kennedy', '...'),
(76, 'Sarah Vaughan', '...'),
(77, 'Patricia Macdonald', '...'),
(78, 'Saskia Sarginson', '...'),
(79, 'John Grisham', '...'),
(80, 'Lisa Gardner', '...'),
(81, 'Jean-Paul Malaval', '...'),
(82, 'Diane Chamberlain', '...'),
(83, 'Nora Roberts', '...'),
(84, 'Stuart Harrison', '...'),
(85, 'Jacquelyn Mitchard', '...'),
(86, 'Yvonne Coudurier', '...'),
(87, 'Véronique Jannot', '...'),
(88, 'Annabelle Descrosiers', '...'),
(89, 'Marc Lavoine', '...'),
(90, 'Michel Honaker', '...'),
(91, 'Elena Mauli Shapiro', '...'),
(92, 'Florence Cassez', '...'),
(93, 'Jacques-Michel Huret', '...'),
(94, 'Michelle Knight', '...'),
(95, 'Stéphane Loisy', '...'),
(96, 'Charlotte Valandrey', '...'),
(97, 'Alex Barclay', '...'),
(98, 'Jodi Picoult', '...'),
(99, 'Olivier Bonnard', '...'),
(100, 'Choderlos de Laclos', '...'),
(101, 'Dominique Richard', '...'),
(102, 'Annie Ernaux', '...'),
(103, 'Ken Follet', '...'),
(104, 'Jacques Prévert', '...'),
(105, 'Laird Koening', '...'),
(106, 'Pierre Corneille', '...'),
(107, 'Voltaire', '...'),
(108, 'Jules Renard', '...'),
(109, 'Marivaud', '...'),
(110, 'Madame de Lafayette', '...'),
(111, 'Paul Claudel', '...'),
(112, 'Samuel Beckett', '...'),
(113, 'Stendhal', '...'),
(114, 'Etienne Delessert', '...'),
(115, 'Michaël Marshall', '...'),
(116, 'Ingrid Chauvin', '...'),
(117, 'Dany Rousson', '...'),
(118, 'Michel Bussi', '...'),
(119, 'Henri Julien', '...');

-- --------------------------------------------------------

--
-- Structure de la table `film`
--

CREATE TABLE `film` (
  `id` int(11) NOT NULL,
  `titre` varchar(100) NOT NULL,
  `id_realisateur` int(11) NOT NULL,
  `id_genre` int(11) NOT NULL,
  `disque_dur` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `genre`
--

CREATE TABLE `genre` (
  `id` int(11) NOT NULL,
  `libelle` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `jeux`
--

CREATE TABLE `jeux` (
  `id` int(11) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `date_sortie` varchar(50) NOT NULL,
  `id_platforme` int(11) NOT NULL,
  `id_support` int(11) DEFAULT NULL,
  `id_type_jeux` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `jeux`
--

INSERT INTO `jeux` (`id`, `nom`, `date_sortie`, `id_platforme`, `id_support`, `id_type_jeux`) VALUES
(1, 'Farming simulator 11', '2011', 1, 5, 5),
(2, 'Farming Simulator 13', '2013', 1, 5, 5),
(3, 'Framing SImulator 15', '2015', 1, 1, 5),
(4, 'Farming Simulator 19', '2019', 1, 2, 5),
(5, 'dd', 'dd', 4, 6, 7);

-- --------------------------------------------------------

--
-- Structure de la table `livre`
--

CREATE TABLE `livre` (
  `ref` int(11) NOT NULL,
  `tome` varchar(2) DEFAULT NULL,
  `titre` varchar(100) DEFAULT NULL,
  `id_auteur` int(11) DEFAULT NULL,
  `id_type` int(11) DEFAULT NULL,
  `localisation` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `livre`
--

INSERT INTO `livre` (`ref`, `tome`, `titre`, `id_auteur`, `id_type`, `localisation`) VALUES
(1, '1', 'Les chemins de poussières : L\'ange de la Mort ', 1, 8, 'ChambreN'),
(2, '2', 'Les chemins de poussières : Sombre Eden', 1, 8, 'ChambreN'),
(3, '3', 'Les chemins de poussières : Etoile Rebelle', 1, 8, 'ChambreN'),
(4, '1', 'Astérix et Obelix : Astérix le Gaulois', 2, 2, 'Salon'),
(5, '2', 'Astérix et Obelix : Le Serpe d\'or', 2, 2, 'Salon'),
(6, '3', 'Astérix et Obelix : Astérix et les Goths', 2, 2, 'Salon'),
(7, '4', 'Astérix et Obelix : Astérix Gladiateur', 2, 2, 'Salon'),
(8, '5', 'Astérix et Obelix : Le Tour de Gaule d\'Astérix', 2, 2, 'Salon'),
(9, '6', 'Astérix et Obelix : Astérix et Cléopatre', 2, 2, 'Salon'),
(10, '7', 'Astérix et Obelix : Le combat des chefs', 2, 2, 'Salon'),
(11, '8', 'Astérix et Obelix : Astérix chez les Bretons', 2, 2, 'Salon'),
(12, '9', 'Astérix et Obelix : Astérix et les Normands', 2, 2, 'Salon'),
(13, '10', 'Astérix et Obelix : Astérieux légionnaire', 2, 2, 'Salon'),
(14, '11', 'Astérix et Obelix : Le bouclier Averne', 2, 2, 'Salon'),
(15, '12', 'Astérix et Obelix : Astérix aux jeux Olympiques', 2, 2, 'Salon'),
(16, '13', 'Astérix et Obelix : Astérix et le chaudron', 2, 2, 'Salon'),
(17, '14', 'Astérix et Obelix : Astérix en Hispanie', 2, 2, 'Salon'),
(18, '15', 'Astérix et Obelix : La Zizanie', 2, 2, 'Salon'),
(19, '16', 'Astérix et Obelix : Astérix chez les Helvètes', 2, 2, 'Salon'),
(20, '17', 'Astérix et Obelix : Le Domaine des DIeux', 2, 2, 'Salon'),
(21, '18', 'Astérix et Obelix : Les Lauriers de César', 2, 2, 'Salon'),
(22, '19', 'Astérix et Obelix : Le Devin', 2, 2, 'Salon'),
(23, '20', 'Astérix et Obelix : Astérix en Corse', 2, 2, 'Salon'),
(24, '22', 'Astérix et Obelix : Le Cadeau de César', 2, 2, 'Salon'),
(25, '23', 'Astérix et Obelix : La Grande Traversée', 2, 2, 'Salon'),
(26, '24', 'Astérix et Obelix : Obélix et Compagnie', 2, 2, 'Salon'),
(27, '25', 'Astérix et Obelix : Astérix chez les Bleges', 2, 2, 'Salon'),
(28, '26', 'Astérix et Obelix : L\'Odyssée d\'Astérix', 2, 2, 'Salon'),
(29, '27', 'Astérix et Obelix : Le Fils d\'Astérix', 2, 2, 'Salon'),
(30, '28', 'Astérix et Obelix : Astérix chez Rahazade', 2, 2, 'Salon'),
(31, '29', 'Astérix et Obelix : La Rose et le Glaive', 2, 2, 'Salon'),
(32, '30', 'Astérix et Obelix : La Galère d\'Obélix', 2, 2, 'Salon'),
(33, '31', 'Astérix et Obelix : Astérix et Latraviata', 2, 2, 'Salon'),
(34, '32', 'Astérix et Obelix : Astérix et la rentrée Gauloise', 2, 2, 'Salon'),
(35, '33', 'Astérix et Obelix : Le Ciel lui tombe sur la tête', 2, 2, 'Salon'),
(36, '34', 'Astérix et Obelix : L\'Anniversaire d\'Astérix & Obélix', 2, 2, 'Salon'),
(37, '35', 'Astérix et Obelix : Astérix chez Pictes', 2, 2, 'Salon'),
(38, '36', 'Astérix et Obelix : Le Papyrus de César', 2, 2, 'Salon'),
(39, '37', 'Astérix et Obelix : Astérix et la Transitalique', 2, 2, 'Salon'),
(40, '38', 'Astérix et Obelix : La Fille de Vercingétorix', 2, 2, 'Salon'),
(41, '', 'Afropean Soul et autres nouvelles', 3, 15, 'MeubleEntréeGauche'),
(42, '', 'Mythologies', 6, 16, 'MeubleEntréeGauche'),
(43, '', 'Romance Nerveuse', 5, 3, 'MeubleEntréeGauche'),
(44, '', 'La ferme des Animaux', 4, 3, 'MeubleEntréeGauche'),
(45, '', 'Les Fables de La Fontaine', 7, 11, 'MeubleEntréeGauche'),
(46, '', 'Le Cycle des Robots', 8, 3, 'MeubleEntréeGauche'),
(47, '', 'Germinal', 9, 3, 'MeubleEntréeGauche'),
(48, '', 'Le Barbier de Séville', 10, 13, 'MeubleEntréeGauche'),
(49, '', 'Des souris et des hommes', 11, 3, 'MeubleEntréeGauche'),
(50, '', 'L\'école des Femmes', 12, 13, 'MeubleEntréeGauche'),
(51, '', 'Andromaque', 13, 13, 'MeubleEntréeGauche'),
(52, '', 'La Poésie Engagée', 14, 14, 'MeubleEntréeGauche'),
(53, '', 'Le philosophe qui n\'était pas sage', 15, 3, 'MeubleEntréeGauche'),
(54, '', 'En voiture Simone !', 16, 17, 'MeubleEntréeGauche'),
(55, '', 'L\'odysée', 17, 18, 'MeubleEntréeGauche'),
(56, '', 'Mémoire d\'outre-tombe', 18, 12, 'MeubleEntréeGauche'),
(57, '', 'Kamo L\'Agence Babel', 19, 6, 'MeubleEntréeGauche'),
(58, '', 'Sobidor', 20, 6, 'MeubleEntréeGauche'),
(59, '', 'Enquête au collège', 21, 5, 'MeubleEntréeGauche'),
(60, '', 'Le chien des Baskerville', 22, 5, 'MeubleEntréeGauche'),
(61, '', 'Au Bonheur des Orgre', 19, 5, 'MeubleEntréeGauche'),
(62, '', 'Monsieur Malayssène', 19, 5, 'MeubleEntréeGauche'),
(63, '', 'La Femme Parfait est une connasse', 23, 3, 'MeubleEntréeGauche'),
(64, '', 'Le baiser', 24, 8, 'MeubleEntréeGauche'),
(65, '', 'Le billet gagnant et deux autres nouvelles', 25, 16, 'MeubleEntréeGauche'),
(66, '', 'Yvain ou le Chevalier au lion', 26, 3, 'MeubleEntréeGauche'),
(67, '', 'Esope', 7, 11, 'MeubleEntréeGauche'),
(68, '', 'L\'homme dans le labyrinthe', 27, 8, 'MeubleEntréeGauche'),
(69, '', 'Phèdre', 13, 13, 'MeubleEntréeGauche'),
(70, '', 'Le Horla', 28, 15, 'MeubleEntréeGauche'),
(71, '', 'Frankenstein', 29, 8, 'MeubleEntréeGauche'),
(72, '', 'Une fille comme elle', 30, 3, 'MeubleEntréeGauche'),
(73, '', 'Le dernier jour d\'un condamné', 31, 3, 'MeubleEntréeGauche'),
(74, '', 'Allô ! Ici le tueur', 32, 6, 'MeubleEntréeGauche'),
(75, '', 'L\'homme à l\'envers', 33, 3, 'MeubleEntréeGauche'),
(76, '', 'Les bavardes : meurtres sur la côte d\'Azur', 34, 3, 'MeubleEntréeGauche'),
(77, '', 'Acquittée', 35, 3, 'MeubleEntréeGauche'),
(78, '', 'Red Paint Bay', 36, 3, 'MeubleEntréeGauche'),
(79, '', 'Les papillons rêvent-ils d\'éternité ?', 37, 12, 'MeubleEntréeGauche'),
(80, '', 'Confidences de stars sur l\'étrange et l\'au-Dela', 38, 19, 'MeubleEntréeGauche'),
(81, '', 'Ma vie à 200 à l\'heure', 39, 12, 'MeubleEntréeGauche'),
(82, '', 'Miss Charity', 40, 20, 'MeubleEntréeGauche'),
(83, '', 'Elle & Lui', 30, 4, 'MeubleEntréeGauche'),
(84, '', 'Le Jout où les vieilles dames parlent aux morts', 41, 3, 'MeubleEntréeGauche'),
(85, '', 'Conte populaire et légendes de Bretagne', 42, 5, 'MeubleEntréeGauche'),
(86, '', 'Accident', 23, 3, 'MeubleEntréeGauche'),
(87, '', 'Je n\'étais qu\'un fou', 43, 3, 'MeubleEntréeGauche'),
(88, '', 'Forces irrésistibles', 24, 4, 'MeubleEntréeGauche'),
(89, '', 'L\'instant Présent', 44, 8, 'MeubleEntréeGauche'),
(90, '', 'Les sirènes de Saint-Malo', 45, 3, 'MeubleEntréeGauche'),
(91, '', 'Elle s\'appelait Sarah', 46, 3, 'MeubleEntréeGauche'),
(92, '', 'Un parfait inconnu', 24, 3, 'MeubleEntréeGauche'),
(93, '', 'La guerre de Bouton', 47, 3, 'MeubleEntréeGauche'),
(94, '', 'Je le ferai pour toi', 43, 3, 'MeubleEntréeGauche'),
(95, '', 'Sans regrets', 45, 4, 'MeubleEntréeGauche'),
(96, '', 'Cette chanson que je n\'oublierai jamais', 48, 3, 'MeubleEntréeGauche'),
(97, '', 'Six ans déjà', 49, 3, 'MeubleEntréeGauche'),
(98, '', 'Facination', 50, 3, 'MeubleEntréeGauche'),
(99, '', 'Madame Bovary', 51, 3, 'MeubleEntréeGauche'),
(100, '', 'Abu Roi', 52, 18, 'MeubleEntréeGauche'),
(101, '', 'Le Roi se meurt', 53, 13, 'MeubleEntréeGauche'),
(102, '', 'Contes du jour et de la nuit', 28, 20, 'MeubleEntréeGauche'),
(103, '', 'Contes Fantastiques Complets', 28, 20, 'MeubleEntréeGauche'),
(104, '', 'Balzac et la Petite Tailleuse chinoise', 54, 3, 'MeubleEntréeGauche'),
(105, '', 'Oeuvres Poétiques', 55, 14, 'MeubleEntréeGauche'),
(106, '', 'Les Lettres de mon moulin', 56, 15, 'MeubleEntréeGauche'),
(107, '', 'Contes et Nouvelles', 28, 15, 'MeubleEntréeGauche'),
(108, '', 'La maison dans la dune', 57, 5, 'MeubleEntréeGauche'),
(109, '', 'L\'enfant', 58, 3, 'MeubleEntréeGauche'),
(110, '', 'Antigone', 59, 13, 'MeubleEntréeGauche'),
(111, '', 'La Métamorphose', 60, 15, 'MeubleEntréeGauche'),
(112, '', 'Stupeur et Tremblements', 61, 3, 'MeubleEntréeGauche'),
(113, '', 'Emile ou de l\'éducation', 62, 21, 'MeubleEntréeGauche'),
(114, '', 'Thérèse Desqueyroux', 63, 3, 'MeubleEntréeGauche'),
(115, '', 'La soupe aux cailloux', 64, 12, 'MeubleEntréeGauche'),
(116, '', 'Croc-Blanc', 65, 3, 'MeubleEntréeGauche'),
(117, '', 'Un roi sans divertissement', 66, 3, 'MeubleEntréeGauche'),
(118, '', 'Alcool', 67, 16, 'MeubleEntréeGauche'),
(119, '', 'Vous ne devinerez jamais', 68, 20, 'MeubleEntréeGauche'),
(120, '', 'La vie est brève et le désir sans fin', 69, 20, 'MeubleEntréeGauche'),
(121, '', 'Une fille fait la noce', 70, 4, 'MeubleEntréeGauche'),
(122, '', 'La vie est trop courte', 71, 3, 'MeubleEntréeGauche'),
(123, '', 'Donne-moi tes yeux', 72, 3, 'MeubleEntréeGauche'),
(124, '', 'Le secret du mari', 73, 20, 'MeubleEntréeGauche'),
(125, '', 'La lionne', 74, 3, 'MeubleEntréeGauche'),
(126, '', 'La Symphonie du hasard', 75, 20, 'MeubleEntréeGauche'),
(127, '', 'La ferme du bout du monde', 76, 20, 'MeubleEntréeGauche'),
(128, '', 'Une mère sous influence', 77, 20, 'MeubleEntréeGauche'),
(129, '', 'Ne t\'éloigne pas', 49, 20, 'MeubleEntréeGauche'),
(130, '', 'Un sentiment plus fort que la peur', 30, 3, 'MeubleEntréeGauche'),
(131, '', 'Balle de match', 49, 3, 'MeubleEntréeGauche'),
(132, '', 'Sans Toi', 78, 20, 'MeubleEntréeGauche'),
(133, '', 'La confession', 79, 5, 'MeubleEntréeGauche'),
(134, '', 'Famille parfaite', 80, 20, 'MeubleEntréeGauche'),
(135, '', 'Les encriers de porcelaine', 81, 20, 'MeubleEntréeGauche'),
(136, '', 'L\'enfant de l\'été', 82, 4, 'MeubleEntréeGauche'),
(137, '', 'Un coupable trop parfait', 77, 20, 'MeubleEntréeGauche'),
(138, '', 'L\'héritage de Clara', 45, 4, 'MeubleEntréeGauche'),
(139, '', 'Par une nuit d\'hiver', 83, 3, 'MeubleEntréeGauche'),
(140, '', 'Le faucon des neiges', 84, 22, 'MeubleEntréeGauche'),
(141, '', 'Un été pas comme les autres', 85, 3, 'MeubleEntréeGauche'),
(142, '', 'Eternels Célibataires', 24, 3, 'MeubleEntréeGauche'),
(143, '', 'Le Billet Gagant', 25, 20, 'MeubleEntréeGauche'),
(144, '', 'Deux petites filles en bleu', 25, 3, 'MeubleEntréeGauche'),
(145, '', 'Le lac d\'Aiguebelette', 86, 3, 'MeubleEntréeGauche'),
(146, '', 'Trouver le chemin', 87, 12, 'MeubleEntréeGauche'),
(147, '', 'Le bal des secrets', 88, 23, 'MeubleEntréeGauche'),
(148, '', 'L\'homme qui ment', 89, 19, 'MeubleEntréeGauche'),
(149, '', 'Trahie', 24, 3, 'MeubleEntréeGauche'),
(150, '', 'Une grande Fille', 24, 3, 'MeubleEntréeGauche'),
(151, '', 'Chasseur Noir', 90, 6, 'MeubleEntréeGauche'),
(152, '', '13, rue Thérèse', 91, 12, 'MeubleEntréeGauche'),
(153, '', 'Rien n\'emprisonne l\'innocence', 92, 12, 'MeubleEntréeGauche'),
(154, '', 'J\'ai oublié 30 de ma vie', 93, 24, 'MeubleEntréeGauche'),
(155, '', 'Traverser l\'enfer et croire encore au paradis', 94, 12, 'MeubleEntréeGauche'),
(156, '', 'Otage à jolo', 95, 25, 'MeubleEntréeGauche'),
(157, '', 'De coeur Inconnu', 96, 20, 'MeubleEntréeGauche'),
(158, '', 'L\'Amour dans le sang', 96, 12, 'MeubleEntréeGauche'),
(159, '', 'Blackrun', 97, 20, 'MeubleEntréeGauche'),
(160, '', 'Pardonne-lui', 98, 4, 'MeubleEntréeGauche'),
(161, '', 'Vilaine Fille', 99, 4, 'MeubleEntréeGauche'),
(162, '', 'La ronde des souvenirs', 24, 3, 'MeubleEntréeGauche'),
(163, '', 'Les liaisons dangereuses', 100, 20, 'MeubleEntréeGauche'),
(164, '', 'L\'aigle solitaire', 24, 4, 'MeubleEntréeGauche'),
(165, '', 'La dernière des Stanfield', 30, 20, 'MeubleEntréeGauche'),
(166, '', 'Paris Retrouvé', 24, 3, 'MeubleEntréeGauche'),
(167, '', 'Le Journal de Grosse Patate', 101, 13, 'MeubleEntréeGauche'),
(168, '', 'La place ', 102, 12, 'MeubleEntréeGauche'),
(169, '', 'Le Réseau Corneille', 103, 26, 'MeubleEntréeGauche'),
(170, '', 'Au Nom Du Père', 45, 12, 'MeubleEntréeGauche'),
(171, '', 'L\'appel de la forêt', 65, 19, 'MeubleEntréeGauche'),
(172, '', 'Paroles', 104, 14, 'MeubleEntréeGauche'),
(173, '', 'La petite fille au bout du chemin', 105, 20, 'MeubleEntréeGauche'),
(174, '', 'Le cid', 106, 13, 'MeubleEntréeGauche'),
(175, '', 'Candide', 107, 21, 'MeubleEntréeGauche'),
(176, '', 'Poil de carotte', 108, 12, 'MeubleEntréeGauche'),
(177, '', 'Une vie', 28, 20, 'MeubleEntréeGauche'),
(178, '', 'Les raisins de la colère', 11, 20, 'MeubleEntréeGauche'),
(179, '', 'Contes de Bécasse', 28, 15, 'MeubleEntréeGauche'),
(180, '', 'Le jeu de l\'amour et du hasard', 109, 17, 'MeubleEntréeGauche'),
(181, '', 'La princesse de Clèves', 110, 3, 'MeubleEntréeGauche'),
(182, '', 'Le misanthrope', 12, 13, 'MeubleEntréeGauche'),
(183, '', 'Le soulier de Satin', 111, 13, 'MeubleEntréeGauche'),
(184, '', 'Choix de textes', 112, 27, 'MeubleEntréeGauche'),
(185, '', 'Le rouge et le noir', 113, 3, 'MeubleEntréeGauche'),
(186, '', 'Le Roman de Renart I', 114, 28, 'MeubleEntréeGauche'),
(187, '', 'La maison du Mystère', 83, 4, 'MeubleEntréeGauche'),
(188, '', 'Un appartement à Paris', 44, 4, 'MeubleEntréeGauche'),
(189, '', 'Les vens mauvais', 115, 20, 'MeubleEntréeGauche'),
(190, '', 'Un si long chemin', 24, 3, 'MeubleEntréeGauche'),
(191, '', 'Boomerang', 46, 20, 'MeubleEntréeGauche'),
(192, '', 'La vagabonde', 24, 3, 'MeubleEntréeGauche'),
(193, '', 'Des amis si proches', 24, 3, 'MeubleEntréeGauche'),
(194, '', 'Qui serais-je sans toi ?', 44, 4, 'MeubleEntréeGauche'),
(195, '', 'Les années perdues', 25, 20, 'MeubleEntréeGauche'),
(196, '', 'Croire au bonheur', 116, 24, 'MeubleEntréeGauche'),
(197, '', 'Les genêts de Saint-Antonin', 117, 4, 'MeubleEntréeGauche'),
(198, '', 'N\'oublier Jamais', 118, 20, 'MeubleEntréeGauche'),
(199, '', 'Le Ranch', 24, 3, 'MeubleEntréeGauche'),
(200, '', 'Au jour le jour', 24, 3, 'MeubleEntréeGauche'),
(201, '', 'Le pardon', 24, 3, 'MeubleEntréeGauche'),
(202, '', 'Villa Numéro 2', 24, 3, 'MeubleEntréeGauche'),
(203, '', 'Le Mariage', 24, 3, 'MeubleEntréeGauche'),
(204, '', 'Les charmes discret de la vie conjugale', 75, 3, 'MeubleEntréeGauche'),
(205, '', 'Derniers Adieux', 80, 20, 'MeubleEntréeGauche'),
(206, '', 'La bâtisse aux amandiers', 119, 3, 'MeubleEntréeGauche'),
(207, '', 'Le secret des fleurs - La Dahlia Bleu', 83, 4, 'MeubleEntréeGauche'),
(208, '', 'Mes amis Mes amours', 30, 4, 'MeubleEntréeGauche'),
(209, '', 'Si c\'était à refaire', 30, 3, 'MeubleEntréeGauche'),
(210, '', 'Si un jour la vie t\'arrache à moi', 43, 4, 'MeubleEntréeGauche');

-- --------------------------------------------------------

--
-- Structure de la table `platforme`
--

CREATE TABLE `platforme` (
  `id` int(11) NOT NULL,
  `nom` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `platforme`
--

INSERT INTO `platforme` (`id`, `nom`) VALUES
(1, 'PC'),
(2, 'XBOX'),
(3, 'PS4'),
(4, 'test');

-- --------------------------------------------------------

--
-- Structure de la table `realisateur`
--

CREATE TABLE `realisateur` (
  `id` int(11) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `prenom` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `support`
--

CREATE TABLE `support` (
  `id` int(11) NOT NULL,
  `libelle` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `support`
--

INSERT INTO `support` (`id`, `libelle`) VALUES
(1, 'CD'),
(2, 'Steam'),
(3, 'Uplay'),
(4, 'Origin'),
(5, 'Download'),
(6, 'Test');

-- --------------------------------------------------------

--
-- Structure de la table `type`
--

CREATE TABLE `type` (
  `id` int(11) NOT NULL,
  `libelle` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `type`
--

INSERT INTO `type` (`id`, `libelle`) VALUES
(1, '..'),
(2, 'Bande Dessinée'),
(3, 'Roman'),
(4, 'Roman d\'Amour'),
(5, 'Roman Policier'),
(6, 'Roman Jeunesse'),
(7, 'Roman Epistolaire'),
(8, 'Science Fiction'),
(9, 'Fantastique'),
(10, 'Horreur'),
(11, 'Fable'),
(12, 'Biographie '),
(13, 'Théatre'),
(14, 'Poésie'),
(15, 'Nouvelle'),
(16, 'Recueil'),
(17, 'Comédie'),
(18, 'Poème'),
(19, 'Récit'),
(20, 'Fiction'),
(21, 'Philosophie'),
(22, 'Parabole'),
(23, 'Drame'),
(24, 'Témoignage'),
(25, 'Journal Intime'),
(26, 'Histoire de Guerre'),
(27, 'non-classé'),
(28, 'préface'),
(29, 'test'),
(30, 'dd');

-- --------------------------------------------------------

--
-- Structure de la table `type_jeux`
--

CREATE TABLE `type_jeux` (
  `id` int(11) NOT NULL,
  `libelle` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `type_jeux`
--

INSERT INTO `type_jeux` (`id`, `libelle`) VALUES
(1, 'FPS'),
(2, 'RPG'),
(3, 'Infiltration / espionnage'),
(4, 'Sport'),
(5, 'Simulation'),
(6, 'Survie'),
(7, 'test');

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

CREATE TABLE `utilisateur` (
  `id` int(11) NOT NULL,
  `user` varchar(50) NOT NULL,
  `pwd` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `utilisateur`
--

INSERT INTO `utilisateur` (`id`, `user`, `pwd`) VALUES
(1, 'test', '1234');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `auteur`
--
ALTER TABLE `auteur`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `film`
--
ALTER TABLE `film`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_realisateur` (`id_realisateur`),
  ADD KEY `id_genre` (`id_genre`);

--
-- Index pour la table `genre`
--
ALTER TABLE `genre`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `jeux`
--
ALTER TABLE `jeux`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_platforme` (`id_platforme`),
  ADD KEY `id_support` (`id_support`),
  ADD KEY `id_type_jeux` (`id_type_jeux`);

--
-- Index pour la table `livre`
--
ALTER TABLE `livre`
  ADD PRIMARY KEY (`ref`),
  ADD KEY `id_auteur` (`id_auteur`),
  ADD KEY `id_type` (`id_type`);

--
-- Index pour la table `platforme`
--
ALTER TABLE `platforme`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `realisateur`
--
ALTER TABLE `realisateur`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `support`
--
ALTER TABLE `support`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `type`
--
ALTER TABLE `type`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `type_jeux`
--
ALTER TABLE `type_jeux`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `auteur`
--
ALTER TABLE `auteur`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=120;
--
-- AUTO_INCREMENT pour la table `film`
--
ALTER TABLE `film`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `genre`
--
ALTER TABLE `genre`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `jeux`
--
ALTER TABLE `jeux`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `livre`
--
ALTER TABLE `livre`
  MODIFY `ref` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=211;
--
-- AUTO_INCREMENT pour la table `platforme`
--
ALTER TABLE `platforme`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `realisateur`
--
ALTER TABLE `realisateur`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `support`
--
ALTER TABLE `support`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pour la table `type`
--
ALTER TABLE `type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT pour la table `type_jeux`
--
ALTER TABLE `type_jeux`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `film`
--
ALTER TABLE `film`
  ADD CONSTRAINT `film_ibfk_1` FOREIGN KEY (`id_realisateur`) REFERENCES `realisateur` (`id`),
  ADD CONSTRAINT `film_ibfk_2` FOREIGN KEY (`id_genre`) REFERENCES `genre` (`id`);

--
-- Contraintes pour la table `jeux`
--
ALTER TABLE `jeux`
  ADD CONSTRAINT `jeux_ibfk_1` FOREIGN KEY (`id_platforme`) REFERENCES `platforme` (`id`),
  ADD CONSTRAINT `jeux_ibfk_2` FOREIGN KEY (`id_support`) REFERENCES `support` (`id`),
  ADD CONSTRAINT `jeux_ibfk_3` FOREIGN KEY (`id_type_jeux`) REFERENCES `type_jeux` (`id`);

--
-- Contraintes pour la table `livre`
--
ALTER TABLE `livre`
  ADD CONSTRAINT `livre_ibfk_1` FOREIGN KEY (`id_auteur`) REFERENCES `auteur` (`id`),
  ADD CONSTRAINT `livre_ibfk_2` FOREIGN KEY (`id_type`) REFERENCES `type` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
