<?php

namespace Nathan\templates;

use Nathan\controllers\router;

class View
{
    private static $template;
    private static $path = "src/templates/";
    private static $variables = [];

    public static function setTemplate($name)
    {
        self::$template = $name;
    }

    public static function bindVariable($name, $value)
    {
        // store value in a dictionnary
        self::$variables[$name] = $value;
    }

    public static function Display()
    {
        $router = new Router();
        $base_path = $router->getBasePath();

        foreach(self::$variables as $name => $value)
        {
            $$name = $value;
        }

        // load template
        require self::$path . self::$template . ".html.php";
    }
}