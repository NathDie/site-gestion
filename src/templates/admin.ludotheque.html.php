<?php require "html_header_admin.html.php"; ?>
<?php require "barre_nav_admin.html.php"; ?>
<div class="container mt-5">
    <h1 class="text-center">Page d'administration de la ludothèque</h1>
</div>

<div class="container mt-5">
    <div class="row">
        <div class="col">
            <form action="<?= $base_path ?>/ludotheque/genres/add" method="POST">
                <div class="form-group">
                    <h2> Ajouter un Genre</h2>
                    <input type="text" name="libelleG" placeholder="Genre du Film" class="form-control mt-2">
                    <button class="btn btn-primary">Ajouter</button>
                </div>
            </form>
            <?php
            $InsertG = $_GET['ajoutG'];
            $VerifG = $_GET['verifG'];
            $goodInsertG = '<span class = "badge badge-success"> <font style = "vertical-align: inherit;"> <font style = "vertical-align: inherit;"> Votre genre a été enregistré </font></{ </span>';
            $badInsertG = '<span class = "badge badge-danger"> <font style = "vertical-align: inherit;"> <font style = "vertical-align: inherit;"> Votre genre n\'a pas été enregistré </font></{ </span>';
            $goodVerifG = '<span class = "badge badge-warning"> <font style = "vertical-align: inherit;"> <font style = "vertical-align: inherit;"> Votre genre est déjà enregistré ! </font></{ </span>';
            if (!empty($InsertG)) {
                if ($InsertG == "true") {
                    echo $goodInsertG;
                } else if ($InsertG == "false") {
                    echo $badInsertG;
                }
            } else if (!empty($VerifG)) {
                echo $goodVerifG;
            }
            ?>
        </div>
        <div class="col">
            <form action="<?= $base_path ?>/ludotheque/realisateurs/add" method="POST">
                <h2> Ajouter un Réalisateur </h2>
                <div class="form-group">
                    <input type="text" name="nomR" placeholder="Nom du réalisateur" class="form-control mt-2">
                    <input type="text" name="prenomR" placeholder="Prénom du réalisateur" class="form-control mt-2">
                </div>
                <button class="btn btn-primary">Ajouter</button>
            </form>
            <?php
            $InsertR = $_GET['ajoutR'];
            $VerifR = $_GET['verifR'];
            $goodInsertR = '<span class = "badge badge-success"> <font style = "vertical-align: inherit;"> <font style = "vertical-align: inherit;"> Votre réalisateur a été enregistré </font></{ </span>';
            $badInsertR = '<span class = "badge badge-danger"> <font style = "vertical-align: inherit;"> <font style = "vertical-align: inherit;"> Votre réalisateur n\'a pas été enregistré </font></{ </span>';
            $goodVerifR = '<span class = "badge badge-warning"> <font style = "vertical-align: inherit;"> <font style = "vertical-align: inherit;"> Votre réalisateur est déjà enregistré ! </font></{ </span>';
            if (!empty($InsertR)) {
                if ($InsertR == "true") {
                    echo $goodInsertR;
                } else if ($InsertR == "false") {
                    echo $badInsertR;
                }
            } else if (!empty($VerifR)) {
                echo $goodVerifR;
            }
            ?>
        </div>
        <div class="col">
            <form action="<?= $base_path ?>/ludotheque/films/add" method="POST">
                <h2> Ajouter un Film</h2>
                <div class="form-group">
                    <input type="text" name="titreF" placeholder="Titre du film" class="form-control mt-2">
                    <input type="text" name="dateF" placeholder="Année de sortie du film" class="form-control mt-2">
                    <select name="auteurF" class="form-control mt-2">
                        <?php foreach ($realisateurs as $realisateur) : ?>
                            <option value="<?= $realisateur['nom'] . ' ' . $realisateur['prenom']; ?>" selected><?= $realisateur['nom'] . ' ' . $realisateur['prenom']; ?></option>
                        <?php endforeach; ?>
                    </select>
                    <select name="genreF" class="form-control mt-2">
                        <?php foreach ($genres as $genre) : ?>
                            <option value="<?= $genre['libelle']; ?>" selected><?= $genre['libelle']; ?></option>
                        <?php endforeach; ?>
                    </select>
                    <input type="text" name="disque_dur" placeholder="Quelle est le disque dur ?" class="form-control mt-2">
                </div>
                <button class="btn btn-primary">Ajouter</button>
            </form>
            <?php
            $InsertF = $_GET['ajoutF'];
            $VerifF = $_GET['verifF'];
            $goodInsertF = '<span class = "badge badge-success"> <font style = "vertical-align: inherit;"> <font style = "vertical-align: inherit;"> Votre film a été enregistré </font></{ </span>';
            $badInsertF = '<span class = "badge badge-danger"> <font style = "vertical-align: inherit;"> <font style = "vertical-align: inherit;"> Votre film n\'a pas été enregistré </font></{ </span>';
            $goodVerifF = '<span class = "badge badge-warning"> <font style = "vertical-align: inherit;"> <font style = "vertical-align: inherit;"> Votre film est déjà enregistré ! </font></{ </span>';
            if (!empty($InsertF)) {
                if ($InsertF == "true") {
                    echo $goodInsertF;
                } else if ($InsertF == "false") {
                    echo $badInsertF;
                }
            } else if (!empty($VerifF)) {
                echo $goodVerifF;
            }
            ?>
        </div>
    </div>
</div>
<?php require "html_footer.html.php"; ?>