<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="">Gaming</a>
    <button class=" navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="<?= $base_path ?>/gaming/jeux">Jeux</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?= $base_path ?>/gaming/platformes">Platformes</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?= $base_path ?>/gaming/supports">Supports</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?= $base_path ?>/gaming/types_jeux">Types</a>
            </li>
        </ul>
        <ul class="navbar-nav">
            <li class="nav-item">
                <?php
                if (!isset($_SESSION['username'])) {
                    echo "<a class='nav-link' href='' data-toggle='modal' data-target='#modalLoginForm'> Connection</a>";
                } else {
                    echo "<a class='nav-link' href='" . $base_path . "/admin/leave'> Deconnection</a>";
                }
                ?>
            </li>
        </ul>
    </div>
</nav>