<?php require "html_header_admin.html.php"; ?>
<?php require "barre_nav_admin.html.php"; ?>
<div class="container mt-5">
    <h1 class="text-center">Page d'administration des jeux vidéos</h1>
    <div class="row">
        <div class="col">
            <form action="<?= $base_path ?>/gaming/types_jeux/add" method="POST">
                <h2> Ajouter un Type</h2>
                <div class="form-group">
                    <input type="text" name="libelleTJ" placeholder="Nom du type" class="form-control mt-2">
                </div>
                <button class="btn btn-primary">Ajouter</button>
            </form>
            <?php
            $InsertTJ = $_GET['ajoutTJ'];
            $VerifTJ = $_GET['verifTJ'];
            $goodInsertTJ = '<span class = "badge badge-success"> <font style = "vertical-align: inherit;"> <font style = "vertical-align: inherit;"> Votre type de jeu a été enregistré </font></{ </span>';
            $badInsertTJ = '<span class = "badge badge-danger"> <font style = "vertical-align: inherit;"> <font style = "vertical-align: inherit;"> Votre type de jeu n\'a pas été enregistré </font></{ </span>';
            $goodVerifTJ = '<span class = "badge badge-warning"> <font style = "vertical-align: inherit;"> <font style = "vertical-align: inherit;"> Votre type de jeu est déjà enregistré ! </font></{ </span>';
            if (!empty($InsertTJ)) {
                if ($InsertTJ == "true") {
                    echo $goodInsertTJ;
                } else if ($InsertTJ == "false") {
                    echo $badInsertTJ;
                }
            } else if (!empty($VerifTJ)) {
                echo $goodVerifTJ;
            }
            ?>
        </div>
        <div class="col">
            <form action="<?= $base_path ?>/gaming/supports/add" method="POST">
                <h2> Ajouter un Support</h2>
                <div class="form-group">
                    <input type="text" name="libelleSJ" placeholder="Nom du support" class="form-control mt-2">
                </div>
                <button class="btn btn-primary">Ajouter</button>
            </form>
            <?php
            $InsertSJ = $_GET['ajoutSJ'];
            $VerifSJ = $_GET['verifSJ'];
            $goodInsertSJ = '<span class = "badge badge-success"> <font style = "vertical-align: inherit;"> <font style = "vertical-align: inherit;"> Votre support de jeu a été enregistré </font></{ </span>';
            $badInsertSJ = '<span class = "badge badge-danger"> <font style = "vertical-align: inherit;"> <font style = "vertical-align: inherit;"> Votre support de jeu n\'a pas été enregistré </font></{ </span>';
            $goodVerifSJ = '<span class = "badge badge-warning"> <font style = "vertical-align: inherit;"> <font style = "vertical-align: inherit;"> Votre support de jeu est déjà enregistré ! </font></{ </span>';
            if (!empty($InsertSJ)) {
                if ($InsertSJ == "true") {
                    echo $goodInsertSJ;
                } else if ($InsertSJ == "false") {
                    echo $badInsertSJ;
                }
            } else if (!empty($VerifSJ)) {
                echo $goodVerifSJ;
            }
            ?>
        </div>
        <div class="col">
            <form action="<?= $base_path ?>/gaming/platformes/add" method="POST">
                <h2> Ajouter une Platforme</h2>
                <div class="form-group">
                    <input type="text" name="nomPJ" placeholder="Nom du support" class="form-control mt-2">
                </div>
                <button class="btn btn-primary">Ajouter</button>
            </form>
            <?php
            $InsertPJ = $_GET['ajoutPJ'];
            $VerifPJ = $_GET['verifPJ'];
            $goodInsertPJ = '<span class = "badge badge-success"> <font style = "vertical-align: inherit;"> <font style = "vertical-align: inherit;"> Votre platforme de jeu a été enregistré </font></{ </span>';
            $badInsertPJ = '<span class = "badge badge-danger"> <font style = "vertical-align: inherit;"> <font style = "vertical-align: inherit;"> Votre platforme de jeu n\'a pas été enregistré </font></{ </span>';
            $goodVerifPJ = '<span class = "badge badge-warning"> <font style = "vertical-align: inherit;"> <font style = "vertical-align: inherit;"> Votre platforme de jeu est déjà enregistré ! </font></{ </span>';
            if (!empty($InsertPJ)) {
                if ($InsertPJ == "true") {
                    echo $goodInsertPJ;
                } else if ($InsertPJ == "false") {
                    echo $badInsertPJ;
                }
            } else if (!empty($VerifPJ)) {
                echo $goodVerifPJ;
            }
            ?>
        </div>
        <div class="col">
            <form action="<?= $base_path ?>/gaming/jeux/add" method="POST">
                <h2> Ajouter un jeu</h2>
                <div class="form-group">
                    <input type="text" name="nomJ" placeholder="Nom du jeu" class="form-control mt-2">
                    <input type="text" name="date_sortie" placeholder="Date de sortie du jeu" class="form-control mt-2">
                    <select name="platformeJ" class="form-control mt-2">
                        <?php foreach ($platformesJ as $platformeJ) : ?>
                            <option value="<?= $platformeJ['nom']; ?>" selected><?= $platformeJ['nom']; ?></option>
                        <?php endforeach; ?>
                    </select>
                    <select name="supportJ" class="form-control mt-2">
                        <?php foreach ($supportsJ as $supportJ) : ?>
                            <option value="<?= $supportJ['libelle']; ?>" selected><?= $supportJ['libelle']; ?></option>
                        <?php endforeach; ?>
                    </select>
                    <select name="type_jeux" class="form-control mt-2">
                        <?php foreach ($types_jeux as $type_jeux) : ?>
                            <option value="<?= $type_jeux['libelle']; ?>" selected><?= $type_jeux['libelle']; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <button class="btn btn-primary">Ajouter</button>
            </form>
            <?php
            $InsertJ = $_GET['ajoutJ'];
            $VerifJ = $_GET['verifJ'];
            $goodInsertJ = '<span class = "badge badge-success"> <font style = "vertical-align: inherit;"> <font style = "vertical-align: inherit;"> Votre jeu a été enregistré </font></{ </span>';
            $badInsertJ = '<span class = "badge badge-danger"> <font style = "vertical-align: inherit;"> <font style = "vertical-align: inherit;"> Votre jeu n\'a pas été enregistré </font></{ </span>';
            $goodVerifJ = '<span class = "badge badge-warning"> <font style = "vertical-align: inherit;"> <font style = "vertical-align: inherit;"> Votre jeu est déjà enregistré ! </font></{ </span>';
            if (!empty($InsertJ)) {
                if ($InsertJ == "true") {
                    echo $goodInsertJ;
                } else if ($InsertJ == "false") {
                    echo $badInsertJ;
                }
            } else if (!empty($VerifJ)) {
                echo $goodVerifJ;
            }
            ?>
        </div>
    </div>
</div>
<?php require "html_footer.html.php"; ?>