<?php require "html_header_admin.html.php"; ?>
<?php require "barre_nav_admin.html.php"; ?>
<div class="container mt-5">
    <h1 class="text-center">Page d'administration de la bibliothèque</h1>
    <div class="row mt-4">
        <div class="col">
            <form action="<?= $base_path ?>/bibliotheque/livres/add" method="POST">
                <h2> Ajouter un Livre</h2>
                <div class="form-group">
                    <input type="text" name="tomeL" placeholder="Tome du livre" class="form-control mt-2">
                    <input type="text" name="nomL" placeholder="Nom du livre" class="form-control mt-2">
                    <select name="auteurL" class="form-control mt-2">
                        <?php foreach ($auteurs as $auteur) : ?>
                            <option value="<?= $auteur['nom']; ?>" selected><?= $auteur['nom']; ?></option>
                        <?php endforeach; ?>
                    </select>
                    <select name="typeL" class="form-control mt-2">
                        <?php foreach ($types as $type) : ?>
                            <option value="<?= $type['libelle']; ?>" selected><?= $type['libelle']; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <button class="btn btn-primary">Ajouter</button>
            </form>
            <?php
            $InsertL = $_GET['ajoutL'];
            $VerifL = $_GET['verifL'];
            $goodInsert = '<span class = "badge badge-success"> <font style = "vertical-align: inherit;"> <font style = "vertical-align: inherit;"> Votre livre a été enregistré </font></{ </span>';
            $badInsert = '<span class = "badge badge-danger"> <font style = "vertical-align: inherit;"> <font style = "vertical-align: inherit;"> Votre livre n\'a pas été enregistré </font></{ </span>';
            $goodVerif = '<span class = "badge badge-warning"> <font style = "vertical-align: inherit;"> <font style = "vertical-align: inherit;"> Votre livre est déjà enregistré ! </font></{ </span>';
            if (!empty($InsertL)) {
                if ($InsertL == "true") {
                    echo $goodInsert;
                } else if ($InsertL == "false") {
                    echo $badInsert;
                }
            } else if (!empty($VerifL)) {
                echo $goodVerif;
            }
            ?>
        </div>
        <div class="col">
            <form action="<?= $base_path ?>/bibliotheque/types/add" method="POST">
                <h2> Ajouter un Type</h2>
                <div class="form-group">
                    <input type="text" name="libelleT" placeholder="Libelle du type" class="form-control">
                </div>
                <button class="btn btn-primary">Ajouter</button>
            </form>
            <?php
            $InsertT = $_GET['ajoutT'];
            $VerifT = $_GET['verifT'];
            $goodInsertT = '<span class = "badge badge-success"> <font style = "vertical-align: inherit;"> <font style = "vertical-align: inherit;"> Votre type a été enregistré </font></{ </span>';
            $badInsertT = '<span class = "badge badge-danger"> <font style = "vertical-align: inherit;"> <font style = "vertical-align: inherit;"> Votre type n\'a pas été enregistré </font></{ </span>';
            $goodVerifT = '<span class = "badge badge-warning"> <font style = "vertical-align: inherit;"> <font style = "vertical-align: inherit;"> Votre type est déjà enregistré ! </font></{ </span>';
            if (!empty($InsertT)) {
                if ($InsertT == "true") {
                    echo $goodInsertT;
                } else if ($InsertT == "false") {
                    echo $badInsertT;
                }
            } else if (!empty($VerifT)) {
                echo $goodVerifT;
            }
            ?>
        </div>
        <div class="col">
            <form action="<?= $base_path ?>/bibliotheque/auteurs/add" method="POST">
                <h2> Ajout un Auteur</h2>
                <div class="form-group">
                    <input type="text" name="nomA" placeholder="Nom de l'auteur" class="form-control mt-2">
                    <textarea name="descriptionA" placeholder="Biographie de l'auteur" class="form-control mt-2"></textarea>
                </div>
                <button class="btn btn-primary" class="mt-2">Ajouter</button>
            </form>
            <?php
            $InsertA = $_GET['ajoutA'];
            $VerifA = $_GET['verifA'];
            $goodInsertA = '<span class = "badge badge-success"> <font style = "vertical-align: inherit;"> <font style = "vertical-align: inherit;"> Votre auteur a été enregistré </font></{ </span>';
            $badInsertA = '<span class = "badge badge-danger"> <font style = "vertical-align: inherit;"> <font style = "vertical-align: inherit;"> Votre auteur n\'a pas été enregistré </font></{ </span>';
            $goodVerifA = '<span class = "badge badge-warning"> <font style = "vertical-align: inherit;"> <font style = "vertical-align: inherit;"> Votre auteur est déjà enregistré ! </font></{ </span>';
            if (!empty($InsertA)) {
                if ($InsertA == "true") {
                    echo $goodInsertA;
                } else if ($InsertA == "false") {
                    echo $badInsertA;
                }
            } else if (!empty($VerifA)) {
                echo $goodVerifA;
            }
            ?>
        </div>
    </div>
</div>

<?php require "html_footer.html.php"; ?>