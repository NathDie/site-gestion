<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="#">Gestionnaire</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="<?= $base_path ?>/bibliotheque" target="_BLANK">Bibliothèque</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?= $base_path ?>/ludotheque" target="_BLANK">Ludothèque</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?= $base_path ?>/gaming" target="_BLANK">Gaming</a>
            </li>
            <?php
            if (!isset($_SESSION['username'])) {
                echo "<li class='nav-item'><a class='nav-link disabled' href='" . $base_path . "/admin/bibliothèque'> Administration</a></li>";
            } else {
                echo "<li class='nav-item'><a class='nav-link' href='" . $base_path . "/admin/bibliothèque'> Administration</a></li>";
            }
            ?>
        </ul>
        <ul class="navbar-nav">
            <li class="nav-item">
                <?php
                if (!isset($_SESSION['username'])) {
                    echo "<a class='nav-link' href='' data-toggle='modal' data-target='#modalLoginForm'> Connection</a>";
                } else {
                    echo "<a class='nav-link' href='" . $base_path . "/admin/leave'> Deconnection</a>";
                }
                ?>
            </li>
        </ul>
    </div>
</nav>