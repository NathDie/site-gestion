<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="">Ludothèque</a>
    <button class=" navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="<?= $base_path ?>/ludotheque/films">Films</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?= $base_path ?>/ludotheque/realisateurs">Réalisateurs</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?= $base_path ?>/ludotheque/genres">Genres</a>
            </li>
        </ul>
        <ul class="navbar-nav">
            <li class="nav-item">
                <?php
                if (!isset($_SESSION['username'])) {
                    echo "<a class='nav-link' href='' data-toggle='modal' data-target='#modalLoginForm'> Connection</a>";
                } else {
                    echo "<a class='nav-link' href='" . $base_path . "/admin/leave'> Deconnection</a>";
                }
                ?>
            </li>
        </ul>
    </div>
</nav>