<?php require "html_header_gaming.html.php"; ?>
<?php require "barre_nav_gaming.html.php"; ?>
<div class="container mt-5">
    <h1 class="text-center">Bienvenue sur la page des types de jeux vidéos</h1>
</div>
<div class="container mt-5">
    <table class="table table-striped table-bordered" style="width:100%" id="type_jeuxD">
        <thead class="thead-dark">
            <tr>
                <th class="">ID</th>
                <th class="">LIBELLE</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($types_jeux as $type_jeux) : ?>
                <tr>
                    <td><?= $type_jeux['id']; ?></td>
                    <td><?= $type_jeux['libelle']; ?></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <script type="text/javascript">
        var $ = jQuery;
        var c;
        $(document).ready(function() {
            $('#type_jeuxD').DataTable();
        });
    </script>
</div>
<div class="modal fade" id="modalLoginForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form action="<?= $base_path ?>/login" method="POST">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h4 class="modal-title w-100 font-weight-bold">Connection</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body mx-3">
                    <div class="md-form mb-5">
                        <i class="fas fa-envelope prefix grey-text"></i>
                        <input type="text" id="defaultForm-text" class="form-control validate" name="username">
                        <label data-error="wrong" data-success="right" for="defaultForm-text">Pseudo</label>
                    </div>

                    <div class="md-form mb-4">
                        <i class="fas fa-lock prefix grey-text"></i>
                        <input type="password" id="defaultForm-pass" class="form-control validate" name="password">
                        <label data-error="wrong" data-success="right" for="defaultForm-pass">Mot de passe</label>
                    </div>

                </div>
                <div class="modal-footer d-flex justify-content-center">
                    <button class="btn btn-default">Connection</button>
                </div>
            </div>
        </form>
    </div>
</div>
<?php require "html_footer.html.php"; ?>