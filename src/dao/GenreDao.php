<?php

namespace Nathan\dao;

use PDO;
use Exception;
use Nathan\dal\Dao;
use Nathan\controllers\Route;
use Nathan\controllers\Router;


class GenreDao
{
    private static $classname = "Nathan\\classes\\Type";
    private static $ctorargs = ["ref", "tome", "titre", "id_auteur", "id_type"];

    public static function get_all()
    {
        $dao = new Dao();
        $dao->open();
        $dbh = $dao->get_dbh();

        $query = "SELECT * FROM `genre`;";
        $sth = $dbh->prepare($query);
        $result = $sth->execute();

        if (!$result) {
            $error = $sth->errorInfo();
            throw new Exception("{$error[0]} : {$error[2]}");
        }

        $sth->setFetchMode(
            PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE,
            self::$classname
        );

        $genres = $sth->fetchAll();
        return $genres;
        $dao->close();
    }
    public static function get_libelle()
    {
        $dao = new Dao();
        $dao->open();
        $dbh = $dao->get_dbh();

        $query = "SELECT libelle FROM `genre`;";
        $sth = $dbh->prepare($query);
        $result = $sth->execute();

        if (!$result) {
            $error = $sth->errorInfo();
            throw new Exception("{$error[0]} : {$error[2]}");
        }

        $sth->setFetchMode(
            PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE,
            self::$classname
        );

        $genres = $sth->fetchAll();
        return $genres;
        $dao->close();
    }

    public static function add($libelle)
    {
        $dao = new Dao();
        $dao->open();
        $dbh = $dao->get_dbh();

        $query = "SELECT libelle FROM `genre` WHERE libelle = :libelleG;";
        $sth = $dbh->prepare($query);
        $sth->bindParam('libelleG', $libelle, PDO::PARAM_STR);
        $sth->execute();
        $result = $sth->fetch();

        if ($result) {
            $router = new Router();
            $base_path = $router->getBasePath();
            header("Location:" . $base_path . "/admin/ludotheque?verifG=true");
        } else {
            $query = "INSERT INTO genre (libelle) VALUES (:libelleG);";
            $sth = $dbh->prepare($query);
            $sth->bindParam('libelleG', $libelle, PDO::PARAM_STR);
            $sth->execute();
            if (!$sth) {
                $router = new Router();
                $base_path = $router->getBasePath();
                header("Location:" . $base_path . "/admin/ludotheque?ajoutG=false");
            } else {
                $router = new Router();
                $base_path = $router->getBasePath();
                header("Location:" . $base_path . "/admin/ludotheque?ajoutG=true");
            }
        }
        $dao->close();
    }
}
