<?php

namespace Nathan\dao;

use PDO;
use Exception;
use Nathan\dal\Dao;
use Nathan\controllers\Route;
use Nathan\controllers\Router;

class LoginDao
{
    private static $classname = "Nathan\\classes\\Livre";
    private static $ctorargs = ["ref", "tome", "titre", "id_auteur", "id_type"];

    public static function get($username, $password)
    {
        $dao = new Dao();
        $dao->open();
        $dbh = $dao->get_dbh();
        $password = hash('sha512', $password);

        $query = "SELECT user FROM utilisateur WHERE user = :username AND pwd = :password;";
        $sth = $dbh->prepare($query);
        $sth->bindParam('username', $username, PDO::PARAM_STR);
        $sth->bindParam('password', $password, PDO::PARAM_STR);
        $sth->execute();
        $result = $sth->fetch();

        if (!$result) {
            /*$error = $sth->error_log();
            var_dump($error);*/
            $router = new Router();
            $base_path = $router->getBasePath();
            header("Location:" . $base_path . "/");
        } else {
            session_start();
            $_SESSION['username'] = $username;
            $router = new Router();
            $base_path = $router->getBasePath();
            header("Location:" . $base_path . "/admin/bibliotheque");
        }

        $sth->setFetchMode(
            PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE,
            self::$classname
        );
        $dao->close();
    }
}
