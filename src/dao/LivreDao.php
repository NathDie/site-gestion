<?php

namespace Nathan\dao;

use PDO;
use Exception;
use Nathan\dal\Dao;
use Nathan\controllers\Router;

class LivreDao
{
    private static $classname = "Nathan\\classes\\Livre";
    private static $ctorargs = ["ref", "tome", "titre", "id_auteur", "id_type"];

    public static function get_all()
    {
        $dao = new Dao();
        $dao->open();
        $dbh = $dao->get_dbh();

        $query = "SELECT l.ref as ref, l.tome as tome, l.titre, auteur.nom as nom_auteur, type.libelle as libelle_type, l.localisation as localisation 
        FROM `livre` l 
        INNER JOIN auteur 
        ON l.id_auteur = auteur.id 
        INNER JOIN `type` 
        ON l.id_type = type.id;";
        $sth = $dbh->prepare($query);
        $result = $sth->execute();

        if (!$result) {
            $error = $sth->errorInfo();
            throw new Exception("{$error[0]} : {$error[2]}");
        }

        $sth->setFetchMode(
            PDO::FETCH_OBJ | PDO::FETCH_PROPS_LATE,
            self::$classname
        );

        $livres = $sth->fetchAll();
        $dao->close();
        return $livres;
    }

    public static function add($tomeL, $nomL, $nomA, $libelleT)
    {
        $dao = new Dao();
        $dao->open();
        $dbh = $dao->get_dbh();

        $queryT = "SELECT id as idType FROM `type` WHERE libelle = :libelletype;";
        $sthT = $dbh->prepare($queryT);
        $sthT->bindParam('libelletype', $libelleT, PDO::PARAM_STR);
        $sthT->execute();
        $resultT = $sthT->fetch();

        if ($resultT) {
            $queryA = "SELECT id as idAuteur FROM `auteur` WHERE nom = :nomauteur;";
            $sthA = $dbh->prepare($queryA);
            $sthA->bindParam('nomauteur', $nomA, PDO::PARAM_STR);
            $sthA->execute();
            $resultA = $sthA->fetch();

            if ($resultA) {
                $query = "SELECT tome, titre, id_auteur, id_type FROM `livre` WHERE tome = :tomelivre AND titre = :nomlivre AND id_auteur = :idA AND id_type = :idT;";
                $sth = $dbh->prepare($query);
                $sth->bindParam('tomelivre', $tomeL, PDO::PARAM_STR);
                $sth->bindParam('nomlivre', $nomL, PDO::PARAM_STR);
                $sth->bindParam('idA', $resultA['idAuteur'], PDO::PARAM_STR);
                $sth->bindParam('idT', $resultT['idType'], PDO::PARAM_STR);
                $sth->execute();
                $result = $sth->fetch();

                if ($result) {
                    $router = new Router();
                    $base_path = $router->getBasePath();
                    header("Location:" . $base_path . "/admin/bibliotheque?verifL=true");
                } else {
                    $queryL = "INSERT INTO livre (tome, titre, id_auteur, id_type) VALUES (:tomelivre, :nomlivre, :idAut, :idTy);";
                    $sthL = $dbh->prepare($queryL);
                    $sthL->bindParam('tomelivre', $tomeL, PDO::PARAM_STR);
                    $sthL->bindParam('nomlivre', $nomL, PDO::PARAM_STR);
                    $sthL->bindParam('idAut', $resultA['idAuteur'], PDO::PARAM_STR);
                    $sthL->bindParam('idTy', $resultT['idType'], PDO::PARAM_STR);
                    $sthL->execute();
                    if (!$sthL) {
                        $router = new Router();
                        $base_path = $router->getBasePath();
                        header("Location:" . $base_path . "/admin/bibliotheque?ajoutL=false");
                    } else {
                        $router = new Router();
                        $base_path = $router->getBasePath();
                        header("Location:" . $base_path . "/admin/bibliotheque?ajoutL=true");
                    }
                }
                $dao->close();
            }
        }
    }
}
