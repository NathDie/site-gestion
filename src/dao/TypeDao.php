<?php

namespace Nathan\dao;

use PDO;
use Exception;
use Nathan\dal\Dao;
use Nathan\controllers\Router;

class TypeDao
{
    private static $classname = "Nathan\\classes\\Type";
    private static $ctorargs = ["ref", "tome", "titre", "id_auteur", "id_type"];

    public static function get_all()
    {
        $dao = new Dao();
        $dao->open();
        $dbh = $dao->get_dbh();

        $query = "SELECT * FROM `type`;";
        $sth = $dbh->prepare($query);
        $result = $sth->execute();

        if (!$result) {
            $error = $sth->errorInfo();
            throw new Exception("{$error[0]} : {$error[2]}");
        }

        $sth->setFetchMode(
            PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE,
            self::$classname
        );

        $types = $sth->fetchAll();
        return $types;
        $dao->close();
    }

    public static function get_libelle()
    {
        $dao = new Dao();
        $dao->open();
        $dbh = $dao->get_dbh();

        $query = "SELECT libelle FROM `type`;";
        $sth = $dbh->prepare($query);
        $result = $sth->execute();

        if (!$result) {
            $error = $sth->errorInfo();
            throw new Exception("{$error[0]} : {$error[2]}");
        }

        $sth->setFetchMode(
            PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE,
            self::$classname
        );

        $types = $sth->fetchAll();
        return $types;
        $dao->close();
    }

    public static function add($libelle)
    {
        $dao = new Dao();
        $dao->open();
        $dbh = $dao->get_dbh();

        $query = "SELECT libelle FROM `type` WHERE libelle = :libelletype;";
        $sth = $dbh->prepare($query);
        $sth->bindParam('libelletype', $libelle, PDO::PARAM_STR);
        $sth->execute();
        $result = $sth->fetch();

        if ($result) {
            $router = new Router();
            $base_path = $router->getBasePath();
            header("Location:" . $base_path . "/admin/bibliotheque?verifT=true");
        } else {
            $query = "INSERT INTO type (libelle) VALUES (:libelletype);";
            $sth = $dbh->prepare($query);
            $sth->bindParam('libelletype', $libelle, PDO::PARAM_STR);
            $sth->execute();
            if (!$sth) {
                $router = new Router();
                $base_path = $router->getBasePath();
                header("Location:" . $base_path . "/admin/bibliotheque?ajoutT=false");
            } else {
                $router = new Router();
                $base_path = $router->getBasePath();
                header("Location:" . $base_path . "/admin/bibliotheque?ajoutT=true");
            }
        }
        $dao->close();
    }
}
