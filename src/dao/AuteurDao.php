<?php

namespace Nathan\dao;

use PDO;
use Exception;
use Nathan\dal\Dao;
use Nathan\controllers\Router;

class AuteurDao
{
    private static $classname = "Nathan\\classes\\Auteur";
    private static $ctorargs = ["ref", "tome", "titre", "id_auteur", "id_type"];

    public static function get_all()
    {
        $dao = new Dao();
        $dao->open();
        $dbh = $dao->get_dbh();

        $query = "SELECT * FROM `auteur`;";
        $sth = $dbh->prepare($query);
        $result = $sth->execute();

        if (!$result) {
            $error = $sth->errorInfo();
            throw new Exception("{$error[0]} : {$error[2]}");
        }

        $sth->setFetchMode(
            PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE,
            self::$classname
        );

        $auteurs = $sth->fetchAll();
        return $auteurs;
        $dao->close();
    }

    public static function get_nom()
    {
        $dao = new Dao();
        $dao->open();
        $dbh = $dao->get_dbh();

        $query = "SELECT nom FROM `auteur`;";
        $sth = $dbh->prepare($query);
        $result = $sth->execute();

        if (!$result) {
            $error = $sth->errorInfo();
            throw new Exception("{$error[0]} : {$error[2]}");
        }

        $sth->setFetchMode(
            PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE,
            self::$classname
        );

        $auteurs = $sth->fetchAll();
        return $auteurs;
        $dao->close();
    }

    public static function add($nom, $description)
    {
        $dao = new Dao();
        $dao->open();
        $dbh = $dao->get_dbh();

        $query = "SELECT nom, biographie FROM `auteur` WHERE nom = :nomauteur AND biographie = :descriptionauteur;";
        $sth = $dbh->prepare($query);
        $sth->bindParam('nomauteur', $nom, PDO::PARAM_STR);
        $sth->bindParam('descriptionauteur', $description, PDO::PARAM_STR);
        $sth->execute();
        $result = $sth->fetch();

        if ($result) {
            $router = new Router();
            $base_path = $router->getBasePath();
            header("Location:" . $base_path . "/admin/bibliotheque?verifA=true");
        } else {
            $query = "INSERT INTO auteur (nom, biographie) VALUES (:nomauteur, :descriptionauteur);";
            $sth = $dbh->prepare($query);
            $sth->bindParam('nomauteur', $nom, PDO::PARAM_STR);
            $sth->bindParam('descriptionauteur', $description, PDO::PARAM_STR);
            $sth->execute();
            if (!$sth) {
                $router = new Router();
                $base_path = $router->getBasePath();
                header("Location:" . $base_path . "/admin/bibliotheque?ajoutA=false");
            } else {
                $router = new Router();
                $base_path = $router->getBasePath();
                header("Location:" . $base_path . "/admin/bibliotheque?ajoutA=true");
            }
        }
        $dao->close();
    }
}
