<?php

namespace Nathan\dao;

use PDO;
use Exception;
use Nathan\dal\Dao;
use Nathan\controllers\Router;

class Type_JeuxDao
{
    private static $classname = "Nathan\\classes\\Type";
    private static $ctorargs = ["ref", "tome", "titre", "id_auteur", "id_type"];

    public static function get_all()
    {
        $dao = new Dao();
        $dao->open();
        $dbh = $dao->get_dbh();

        $query = "SELECT * FROM `type_jeux`;";
        $sth = $dbh->prepare($query);
        $result = $sth->execute();

        if (!$result) {
            $error = $sth->errorInfo();
            throw new Exception("{$error[0]} : {$error[2]}");
        }

        $sth->setFetchMode(
            PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE,
            self::$classname
        );

        $type_jeux = $sth->fetchAll();
        return $type_jeux;
        $dao->close();
    }
    public static function get_libelle()
    {
        $dao = new Dao();
        $dao->open();
        $dbh = $dao->get_dbh();

        $query = "SELECT libelle FROM `type_jeux`;";
        $sth = $dbh->prepare($query);
        $result = $sth->execute();

        if (!$result) {
            $error = $sth->errorInfo();
            throw new Exception("{$error[0]} : {$error[2]}");
        }

        $sth->setFetchMode(
            PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE,
            self::$classname
        );

        $type_jeux = $sth->fetchAll();
        return $type_jeux;
        $dao->close();
    }

    public static function add($libelleTJ)
    {
        $dao = new Dao();
        $dao->open();
        $dbh = $dao->get_dbh();

        $query = "SELECT libelle FROM `type_jeux` WHERE libelle = :libelletype_jeux;";
        $sth = $dbh->prepare($query);
        $sth->bindParam('libelletype_jeux', $libelleTJ, PDO::PARAM_STR);
        $sth->execute();
        $result = $sth->fetch();

        if ($result) {
            $router = new Router();
            $base_path = $router->getBasePath();
            header("Location:" . $base_path . "/admin/gaming?verifTJ=true");
        } else {
            $query = "INSERT INTO type_jeux (libelle) VALUES (:libelletype_jeux);";
            $sth = $dbh->prepare($query);
            $sth->bindParam('libelletype_jeux', $libelleTJ, PDO::PARAM_STR);
            $sth->execute();
            if (!$sth) {
                $router = new Router();
                $base_path = $router->getBasePath();
                header("Location:" . $base_path . "/admin/gaming?ajoutTJ=false");
            } else {
                $router = new Router();
                $base_path = $router->getBasePath();
                header("Location:" . $base_path . "/admin/gaming?ajoutTJ=true");
            }
        }
        $dao->close();
    }
}
