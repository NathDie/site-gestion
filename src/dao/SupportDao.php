<?php

namespace Nathan\dao;

use PDO;
use Exception;
use Nathan\dal\Dao;
use Nathan\controllers\Router;

class SupportDao
{
    private static $classname = "Nathan\\classes\\Type";
    private static $ctorargs = ["ref", "tome", "titre", "id_auteur", "id_type"];

    public static function get_all()
    {
        $dao = new Dao();
        $dao->open();
        $dbh = $dao->get_dbh();

        $query = "SELECT * FROM `support`;";
        $sth = $dbh->prepare($query);
        $result = $sth->execute();

        if (!$result) {
            $error = $sth->errorInfo();
            throw new Exception("{$error[0]} : {$error[2]}");
        }

        $sth->setFetchMode(
            PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE,
            self::$classname
        );

        $supports = $sth->fetchAll();
        return $supports;
        $dao->close();
    }

    public static function get_libelle()
    {
        $dao = new Dao();
        $dao->open();
        $dbh = $dao->get_dbh();

        $query = "SELECT libelle FROM `support`;";
        $sth = $dbh->prepare($query);
        $result = $sth->execute();

        if (!$result) {
            $error = $sth->errorInfo();
            throw new Exception("{$error[0]} : {$error[2]}");
        }

        $sth->setFetchMode(
            PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE,
            self::$classname
        );

        $supports = $sth->fetchAll();
        return $supports;
        $dao->close();
    }

    public static function add($libelleSJ)
    {
        $dao = new Dao();
        $dao->open();
        $dbh = $dao->get_dbh();

        $query = "SELECT libelle FROM `support` WHERE libelle = :libellesupport;";
        $sth = $dbh->prepare($query);
        $sth->bindParam('libellesupport', $libelleSJ, PDO::PARAM_STR);
        $sth->execute();
        $result = $sth->fetch();

        if ($result) {
            $router = new Router();
            $base_path = $router->getBasePath();
            header("Location:" . $base_path . "/admin/gaming?verifSJ=true");
        } else {
            $query = "INSERT INTO support (libelle) VALUES (:libellesupport);";
            $sth = $dbh->prepare($query);
            $sth->bindParam('libellesupport', $libelleSJ, PDO::PARAM_STR);
            $sth->execute();
            if (!$sth) {
                $router = new Router();
                $base_path = $router->getBasePath();
                header("Location:" . $base_path . "/admin/gaming?ajoutSJ=false");
            } else {
                $router = new Router();
                $base_path = $router->getBasePath();
                header("Location:" . $base_path . "/admin/gaming?ajoutSJ=true");
            }
        }
        $dao->close();
    }
}
