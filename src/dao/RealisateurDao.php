<?php

namespace Nathan\dao;

use PDO;
use Exception;
use Nathan\dal\Dao;
use Nathan\controllers\Route;
use Nathan\controllers\Router;


class RealisateurDao
{
    private static $classname = "Nathan\\classes\\Type";
    private static $ctorargs = ["ref", "tome", "titre", "id_auteur", "id_type"];

    public static function get_all()
    {
        $dao = new Dao();
        $dao->open();
        $dbh = $dao->get_dbh();

        $query = "SELECT * FROM `realisateur`;";
        $sth = $dbh->prepare($query);
        $result = $sth->execute();

        if (!$result) {
            $error = $sth->errorInfo();
            throw new Exception("{$error[0]} : {$error[2]}");
        }

        $sth->setFetchMode(
            PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE,
            self::$classname
        );

        $realisateurs = $sth->fetchAll();
        return $realisateurs;
        $dao->close();
    }

    public static function get_infos()
    {
        $dao = new Dao();
        $dao->open();
        $dbh = $dao->get_dbh();

        $query = "SELECT nom, prenom FROM `realisateur`;";
        $sth = $dbh->prepare($query);
        $result = $sth->execute();

        if (!$result) {
            $error = $sth->errorInfo();
            throw new Exception("{$error[0]} : {$error[2]}");
        }

        $sth->setFetchMode(
            PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE,
            self::$classname
        );

        $realisateurs = $sth->fetchAll();
        return $realisateurs;
        $dao->close();
    }
    public static function add($nomR, $prenomR)
    {
        $dao = new Dao();
        $dao->open();
        $dbh = $dao->get_dbh();

        $query = "SELECT nom, prenom FROM realisateur WHERE nom = :nomR AND prenom = :prenomR;";
        $sth = $dbh->prepare($query);
        $sth->bindParam('nomR', $nomR, PDO::PARAM_STR);
        $sth->bindParam('prenomR', $prenomR, PDO::PARAM_STR);
        $sth->execute();
        $result = $sth->fetch();

        if ($result) {
            $router = new Router();
            $base_path = $router->getBasePath();
            header("Location:" . $base_path . "/admin/ludotheque?verifR=true");
        } else {
            $query = "INSERT INTO realisateur (nom, prenom) VALUES (:nomR, :prenomR);";
            $sth = $dbh->prepare($query);
            $sth->bindParam('nomR', $nomR, PDO::PARAM_STR);
            $sth->bindParam('prenomR', $prenomR, PDO::PARAM_STR);
            $sth->execute();
            if (!$sth) {
                $router = new Router();
                $base_path = $router->getBasePath();
                header("Location:" . $base_path . "/admin/ludotheque?ajoutR=false");
            } else {
                $router = new Router();
                $base_path = $router->getBasePath();
                header("Location:" . $base_path . "/admin/ludotheque?ajoutR=true");
            }
        }
        $dao->close();
    }
}
