<?php

namespace Nathan\dao;

use PDO;
use Exception;
use Nathan\dal\Dao;
use Nathan\controllers\Router;

class JeuxDao
{
    private static $classname = "Nathan\\classes\\Livre";
    private static $ctorargs = ["ref", "tome", "titre", "id_auteur", "id_type"];

    public static function get_all()
    {
        $dao = new Dao();
        $dao->open();
        $dbh = $dao->get_dbh();

        $query = "SELECT j.id as id, j.nom as nom, j.date_sortie as date_sortie, platforme.nom as nom_platforme, support.libelle as libelle_support, type_jeux.libelle as libelle_type_jeux 
        FROM `jeux` j 
        INNER JOIN platforme 
        ON j.id_platforme = platforme.id 
        INNER JOIN `support` 
        ON j.id_support = support.id
        INNER JOIN type_jeux
        ON j.id_type_jeux = type_jeux.id;";
        $sth = $dbh->prepare($query);
        $result = $sth->execute();

        if (!$result) {
            $error = $sth->errorInfo();
            throw new Exception("{$error[0]} : {$error[2]}");
        }

        $sth->setFetchMode(
            PDO::FETCH_OBJ | PDO::FETCH_PROPS_LATE,
            self::$classname
        );

        $jeux = $sth->fetchAll();
        $dao->close();
        return $jeux;
    }
    public static function add($nomJ, $dateJ, $supportJ, $type_jeux, $platformeJ)
    {
        $dao = new Dao();
        $dao->open();
        $dbh = $dao->get_dbh();

        $queryT = "SELECT id as idType_Jeux FROM `type_jeux` WHERE libelle = :libelletype_jeux;";
        $sthT = $dbh->prepare($queryT);
        $sthT->bindParam('libelletype_jeux', $type_jeux, PDO::PARAM_STR);
        $sthT->execute();
        $resultT = $sthT->fetch();

        if ($resultT) {
            $queryS = "SELECT id as idSupport FROM `support` WHERE libelle = :libellesupport;";
            $sthS = $dbh->prepare($queryS);
            $sthS->bindParam('libellesupport', $supportJ, PDO::PARAM_STR);
            $sthS->execute();
            $resultS = $sthS->fetch();

            if ($resultS) {
                $queryP = "SELECT id as idPlatforme FROM `platforme` WHERE nom = :nomplatforme;";
                $sthP = $dbh->prepare($queryP);
                $sthP->bindParam('nomplatforme', $platformeJ, PDO::PARAM_STR);
                $sthP->execute();
                $resultP = $sthP->fetch();

                if ($resultP) {
                    $query = "SELECT nom, date_sortie, id_platforme, id_support, id_type_jeux FROM `jeux` WHERE nom = :nomJ AND date_sortie = :dateJ AND id_platforme = :idP AND id_support = :idS AND id_type_jeux =  :idTJ;";
                    $sth = $dbh->prepare($query);
                    $sth->bindParam('nomJ', $nomJ, PDO::PARAM_STR);
                    $sth->bindParam('dateJ', $dateJ, PDO::PARAM_STR);
                    $sth->bindParam('idP', $resultP['idPlatforme'], PDO::PARAM_STR);
                    $sth->bindParam('idS', $resultS['idSupport'], PDO::PARAM_STR);
                    $sth->bindParam('idTJ', $resultT['idType_Jeux'], PDO::PARAM_STR);
                    $sth->execute();
                    $result = $sth->fetch();

                    if ($result) {
                        $router = new Router();
                        $base_path = $router->getBasePath();
                        header("Location:" . $base_path . "/admin/gaming?verifJ=true");
                    } else {
                        $queryL = "INSERT INTO jeux (nom, date_sortie, id_platforme, id_support, id_type_jeux) VALUES (:nomJ, :dateJ, :idP, :idS, :idTJ);";
                        $sthJ = $dbh->prepare($queryL);
                        $sthJ->bindParam('nomJ', $nomJ, PDO::PARAM_STR);
                        $sthJ->bindParam('dateJ', $dateJ, PDO::PARAM_STR);
                        $sthJ->bindParam('idP', $resultP['idPlatforme'], PDO::PARAM_STR);
                        $sthJ->bindParam('idS', $resultS['idSupport'], PDO::PARAM_STR);
                        $sthJ->bindParam('idTJ', $resultT['idType_Jeux'], PDO::PARAM_STR);
                        $sthJ->execute();
                        if (!$sthJ) {
                            $router = new Router();
                            $base_path = $router->getBasePath();
                            header("Location:" . $base_path . "/admin/gaming?ajoutJ=false");
                        } else {
                            $router = new Router();
                            $base_path = $router->getBasePath();
                            header("Location:" . $base_path . "/admin/gaming?ajoutJ=true");
                        }
                    }
                    $dao->close();
                }
            }
        }
    }
}
