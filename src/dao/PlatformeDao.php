<?php

namespace Nathan\dao;

use PDO;
use Exception;
use Nathan\dal\Dao;
use Nathan\controllers\Router;

class PlatformeDao
{
    private static $classname = "Nathan\\classes\\Type";
    private static $ctorargs = ["ref", "tome", "titre", "id_auteur", "id_type"];

    public static function get_all()
    {
        $dao = new Dao();
        $dao->open();
        $dbh = $dao->get_dbh();

        $query = "SELECT * FROM `platforme`;";
        $sth = $dbh->prepare($query);
        $result = $sth->execute();

        if (!$result) {
            $error = $sth->errorInfo();
            throw new Exception("{$error[0]} : {$error[2]}");
        }

        $sth->setFetchMode(
            PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE,
            self::$classname
        );

        $platformes = $sth->fetchAll();
        return $platformes;
        $dao->close();
    }

    public static function get_nom()
    {
        $dao = new Dao();
        $dao->open();
        $dbh = $dao->get_dbh();

        $query = "SELECT nom FROM `platforme`;";
        $sth = $dbh->prepare($query);
        $result = $sth->execute();

        if (!$result) {
            $error = $sth->errorInfo();
            throw new Exception("{$error[0]} : {$error[2]}");
        }

        $sth->setFetchMode(
            PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE,
            self::$classname
        );

        $platformes = $sth->fetchAll();
        return $platformes;
        $dao->close();
    }

    public static function add($nomR, $prenomR)
    {
        $dao = new Dao();
        $dao->open();
        $dbh = $dao->get_dbh();

        $query = "SELECT nom, prenom FROM `realisateur` WHERE nom = :nomR AND prenom = :prenomR;";
        $sth = $dbh->prepare($query);
        $sth->bindParam('nomR', $nomR, PDO::PARAM_STR);
        $sth->bindParam('prenomR', $prenomR, PDO::PARAM_STR);
        $sth->execute();
        $result = $sth->fetch();

        if ($result) {
            $router = new Router();
            $base_path = $router->getBasePath();
            header("Location:" . $base_path . "/admin/bibliotheque?verifR=true");
        } else {
            $query = "INSERT INTO type (nom, prenom) VALUES (:nomR, :prenomR);";
            $sth = $dbh->prepare($query);
            $sth->bindParam('nomR', $nomR, PDO::PARAM_STR);
            $sth->bindParam('prenomR', $prenomR, PDO::PARAM_STR);
            $sth->execute();
            if (!$sth) {
                $router = new Router();
                $base_path = $router->getBasePath();
                header("Location:" . $base_path . "/admin/bibliotheque?ajoutR=false");
            } else {
                $router = new Router();
                $base_path = $router->getBasePath();
                header("Location:" . $base_path . "/admin/bibliotheque?ajoutR=true");
            }
        }
        $dao->close();
    }
}
