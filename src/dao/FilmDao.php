<?php

namespace Nathan\dao;

use PDO;
use Exception;
use Nathan\dal\Dao;
use Nathan\controllers\Router;

class FilmDao
{
    private static $classname = "Nathan\\classes\\Livre";
    private static $ctorargs = ["ref", "tome", "titre", "id_auteur", "id_type"];

    public static function get_all()
    {
        $dao = new Dao();
        $dao->open();
        $dbh = $dao->get_dbh();

        $query = "SELECT f.id as id, f.titre as titre, f.annee as annee, realisateur.nom as nom_realisateur, realisateur.prenom as prenom_realisateur, genre.libelle as libelle_genre, f.disque_dur as disque_dur 
        FROM film f
        INNER JOIN realisateur 
        ON f.id_realisateur = realisateur.id 
        INNER JOIN genre 
        ON f.id_genre = genre.id;";
        $sth = $dbh->prepare($query);
        $result = $sth->execute();

        if (!$result) {
            $error = $sth->errorInfo();
            throw new Exception("{$error[0]} : {$error[2]}");
        }

        $sth->setFetchMode(
            PDO::FETCH_OBJ | PDO::FETCH_PROPS_LATE,
            self::$classname
        );

        $films = $sth->fetchAll();
        $dao->close();
        return $films;
    }

    public static function add($titreF, $dateF, $nomR, $libelleG, $disque_dur)
    {
        $dao = new Dao();
        $dao->open();
        $dbh = $dao->get_dbh();

        $queryR = "SELECT id as idRealisateur FROM realisateur WHERE CONCAT(nom, ' ', prenom) = :nomR;";
        $sthR = $dbh->prepare($queryR);
        $sthR->bindParam('nomR', $nomR, PDO::PARAM_STR);
        $sthR->execute();
        $resultR = $sthR->fetch();

        if ($resultR) {
            $queryG = "SELECT id as idGenre FROM genre WHERE libelle = :libelleG;";
            $sthG = $dbh->prepare($queryG);
            $sthG->bindParam('libelleG', $libelleG, PDO::PARAM_STR);
            $sthG->execute();
            $resultG = $sthG->fetch();

            if ($resultG) {
                $query = "SELECT titre, annee, id_realisateur, id_genre FROM film WHERE titre = :titreF AND annee = :anneeF AND id_realisateur = :idR AND id_genre = :idG;";
                $sth = $dbh->prepare($query);
                $sth->bindParam('titreF', $titreF, PDO::PARAM_STR);
                $sth->bindParam('anneeF', $dateF, PDO::PARAM_STR);
                $sth->bindParam('idR', $resultR['idRealisateur'], PDO::PARAM_STR);
                $sth->bindParam('idG', $resultG['idGenre'], PDO::PARAM_STR);
                $sth->execute();
                $result = $sth->fetch();

                if ($result) {
                    $router = new Router();
                    $base_path = $router->getBasePath();
                    header("Location:" . $base_path . "/admin/ludotheque?verifF=true");
                } else {
                    $queryL = "INSERT INTO film (titre, annee, id_realisateur, id_genre, disque_dur) VALUES (:titreF, :anneeF, :idR, :idG, :dd);";
                    $sthL = $dbh->prepare($queryL);
                    $sthL->bindParam('titreF', $titreF, PDO::PARAM_STR);
                    $sthL->bindParam('anneeF', $dateF, PDO::PARAM_STR);
                    $sthL->bindParam('idR', $resultR['idRealisateur'], PDO::PARAM_STR);
                    $sthL->bindParam('idG', $resultG['idGenre'], PDO::PARAM_STR);
                    $sthL->bindParam(':dd', $disque_dur, PDO::PARAM_STR);
                    $sthL->execute();
                    if (!$sthL) {
                        $router = new Router();
                        $base_path = $router->getBasePath();
                        header("Location:" . $base_path . "/admin/ludotheque?ajoutF=false");
                    } else {
                        $router = new Router();
                        $base_path = $router->getBasePath();
                        header("Location:" . $base_path . "/admin/ludotheque?ajoutF=true");
                    }
                }
                $dao->close();
            }
        }
    }
}
