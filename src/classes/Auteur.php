<?php

use Nathan\classes\BusinessObject;
use Nathan\dao\AuteurDao;

class Auteur extends BusinessObject
{
    private $id;
    public function get_id()
    {
        return $this->id;
    }
    public function set_id($value)
    {
        $this->id = $value;
    }

    private $nom;
    public function get_nom()
    {
        return $this->nom;
    }
    public function set_tome($value)
    {
        $this->nom = $value;
    }

    private $biographie;
    public function get_biographie()
    {
        return $this->biographie;
    }
    public function set_biographie($value)
    {
        $this->biographie = $value;
    }

    protected function get_object_vars(): array
    {
        return get_object_vars($this);
    }

    public static function get_all()
    {
        return AuteurDao::get_all();
    }
}
