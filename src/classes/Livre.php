<?php

use Nathan\dao\LivreDao;
use Nathan\classes\BusinessObject;

class Livre extends BusinessObject
{
    private $ref;
    public function get_ref()
    {
        return $this->ref;
    }
    public function set_ref($value)
    {
        $this->ref = $value;
    }

    private $tome;
    public function get_tome()
    {
        return $this->tome;
    }
    public function set_tome($value)
    {
        $this->tome = $value;
    }

    private $titre;
    public function get_titre()
    {
        return $this->titre;
    }
    public function set_titre($value)
    {
        $this->titre = $value;
    }

    private $id_auteur;
    public function get_id_auteur()
    {
        return $this->id_auteur;
    }
    public function set_id_auteur($value)
    {
        $this->id_auteur = $value;
    }

    private $id_type;
    public function get_id_type()
    {
        return $this->id_type;
    }
    public function set_id_type($value)
    {
        $this->id_type = $value;
    }
    private $localisation;
    public function get_localisation()
    {
        return $this->localisation;
    }
    public function set_localisation($value)
    {
        $this->localisation = $value;
    }


    protected function get_object_vars(): array
    {
        return get_object_vars($this);
    }

    //public static function insert()
    //{
    //    return LivreDao::insert();
    //}

    public static function get_all()
    {
        return LivreDao::get_all();
    }
}
