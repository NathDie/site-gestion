<?php

namespace Nathan\controllers;

interface IController
{
    static function route();
}
