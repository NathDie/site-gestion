<?php

namespace Nathan\controllers;

use Nathan\templates\View;
use Nathan\controllers\Route;
use Nathan\controllers\Router;
use Nathan\dao\RealisateurDao;

class RealisateurController implements IController
{
    public static function route()
    {

        $router = new Router();
        $router->addRoute(new Route("/ludotheque/realisateurs", "RealisateurController", "realisateurAction"));
        $router->addRoute(new Route("/ludotheque/realisateurs/add", "RealisateurController", "realisateur_addAction"));
        $route = $router->findRoute();

        if ($route) {
            $route->execute();
        } else {
            View::setTemplate("error404");
            View::display();
        }
    }

    public static function realisateurAction()
    {
        $realisateurs = RealisateurDao::get_all();
        View::setTemplate("ludotheque.realisateur");
        View::bindVariable("realisateurs", $realisateurs);
        View::display();
    }

    public static function realisateur_addAction()
    {
        $nomR = $_POST['nomR'];
        $prenomR = $_POST['prenomR'];

        if (!empty($nomR) && !empty($prenomR)) {
            $libelle = RealisateurDao::add($nomR, $prenomR);
        } else {
            $router = new Router();
            $base_path = $router->getBasePath();
            header("Location:" . $base_path . "/admin/ludotheque?ajoutR=false");
        }
    }
}
