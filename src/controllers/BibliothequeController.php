<?php

namespace Nathan\controllers;

use Nathan\templates\View;
use Nathan\controllers\Route;
use Nathan\controllers\Router;

class BibliothequeController implements IController
{
    public static function route()
    {

        $router = new Router();
        $router->addRoute(new Route("/bibliotheque", "BibliothequeController", "bibliothequeAction"));
        $route = $router->findRoute();

        if ($route) {
            $route->execute();
        } else {
            View::setTemplate("error404");
            View::display();
        }
    }

    public static function bibliothequeAction()
    {
        View::setTemplate("bibliotheque");
        View::display();
    }
}
