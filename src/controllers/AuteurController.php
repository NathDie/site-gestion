<?php

namespace Nathan\controllers;

use Nathan\templates\View;
use Nathan\controllers\Route;
use Nathan\controllers\Router;
use Nathan\dao\AuteurDao;

class AuteurController implements IController
{
    public static function route()
    {

        $router = new Router();
        $router->addRoute(new Route("/bibliotheque/auteurs", "AuteurController", "auteurAction"));
        $router->addRoute(new Route("/bibliotheque/auteurs/add", "AuteurController", "auteur_addAction"));
        $route = $router->findRoute();

        if ($route) {
            $route->execute();
        } else {
            View::setTemplate("error404");
            View::display();
        }
    }

    public static function auteurAction()
    {
        $auteurs = AuteurDao::get_all();
        View::bindVariable("auteurs", $auteurs);
        View::setTemplate("bibliotheque.auteur");
        View::display();
    }

    public static function auteur_addAction()
    {
        $nomA = $_POST['nomA'];
        $descriptionA = $_POST['descriptionA'];

        if (!empty($nomA) && !empty($descriptionA)) {
            $auteur = AuteurDao::add($nomA, $descriptionA);
        } else {
            $router = new Router();
            $base_path = $router->getBasePath();
            header("Location:" . $base_path . "/admin/bibliotheque?ajoutA=false");
        }
    }
}
