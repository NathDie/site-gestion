<?php

namespace Nathan\controllers;

use Nathan\templates\View;
use Nathan\controllers\Route;
use Nathan\controllers\Router;
use Nathan\dao\AuteurDao;
use Nathan\dao\TypeDao;

class TypeController implements IController
{
    public static function route()
    {

        $router = new Router();
        $router->addRoute(new Route("/bibliotheque/types", "TypeController", "typeAction"));
        $router->addRoute(new Route("/bibliotheque/types/add", "TypeController", "type_addAction"));
        $route = $router->findRoute();

        if ($route) {
            $route->execute();
        } else {
            View::setTemplate("error404");
            View::display();
        }
    }

    public static function typeAction()
    {
        $types = TypeDao::get_all();
        View::setTemplate("bibliotheque.type");
        View::bindVariable("types", $types);
        View::display();
    }
    public static function type_addAction()
    {
        $libelleT = $_POST['libelleT'];

        if (!empty($libelleT)) {
            $libelle = TypeDao::add($libelleT);
        } else {
            $router = new Router();
            $base_path = $router->getBasePath();
            header("Location:" . $base_path . "/admin/bibliotheque?ajoutT=false");
        }
    }
}
