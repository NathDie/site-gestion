<?php

namespace Nathan\controllers;

use Nathan\templates\View;
use Nathan\controllers\Route;
use Nathan\controllers\Router;
use Nathan\dao\SupportDao;

class SupportController implements IController
{
    public static function route()
    {

        $router = new Router();
        $router->addRoute(new Route("/gaming/supports", "SupportController", "supportAction"));
        $router->addRoute(new Route("/gaming/supports/add", "SupportController", "support_addAction"));
        $route = $router->findRoute();

        if ($route) {
            $route->execute();
        } else {
            View::setTemplate("error404");
            View::display();
        }
    }

    public static function supportAction()
    {
        $supports = SupportDao::get_all();
        View::setTemplate("gaming.support");
        View::bindVariable("supports", $supports);
        View::display();
    }
    public static function support_addAction()
    {
        $libelleSJ = $_POST['libelleSJ'];

        if (!empty($libelleSJ)) {
            $libelle = SupportDao::add($libelleSJ);
        } else {
            $router = new Router();
            $base_path = $router->getBasePath();
            header("Location:" . $base_path . "/admin/support?ajoutSJ=false");
        }
    }
}
