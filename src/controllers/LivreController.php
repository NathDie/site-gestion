<?php

namespace Nathan\controllers;

use Nathan\templates\View;
use Nathan\controllers\Route;
use Nathan\controllers\Router;
use Nathan\dao\LivreDao;

class LivreController implements IController
{
    public static function route()
    {

        $router = new Router();
        $router->addRoute(new Route("/bibliotheque/livres", "LivreController", "livreAction"));
        $router->addRoute(new Route("/bibliotheque/livres/add", "LivreController", "livre_addAction"));
        $route = $router->findRoute();

        if ($route) {
            $route->execute();
        } else {
            View::setTemplate("error404");
            View::display();
        }
    }

    public static function livreAction()
    {
        $livres = LivreDao::get_all();
        View::setTemplate("bibliotheque.livre");
        View::bindVariable("livres", $livres);
        View::display();
    }
    public static function livre_addAction()
    {
        $tomeL = $_POST['tomeL'];
        $nomL = $_POST['nomL'];
        $nomA = $_POST['auteurL'];
        $typeT = $_POST['typeL'];

        if (!empty($tomeL) && !empty($nomL) && !empty($nomA) && !empty($typeT)) {
            $livre = LivreDao::add($tomeL, $nomL, $nomA, $typeT);
        } else {
            $router = new Router();
            $base_path = $router->getBasePath();
            header("Location:" . $base_path . "/admin/bibliotheque?ajoutL=false");
        }
    }
}
