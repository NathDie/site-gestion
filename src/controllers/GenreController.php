<?php

namespace Nathan\controllers;

use Nathan\templates\View;
use Nathan\controllers\Route;
use Nathan\controllers\Router;
use Nathan\dao\GenreDao;

class GenreController implements IController
{
    public static function route()
    {

        $router = new Router();
        $router->addRoute(new Route("/ludotheque/genres", "GenreController", "genreAction"));
        $router->addRoute(new Route("/ludotheque/genres/add", "GenreController", "genre_addAction"));
        $route = $router->findRoute();

        if ($route) {
            $route->execute();
        } else {
            View::setTemplate("error404");
            View::display();
        }
    }

    public static function genreAction()
    {
        $genres = GenreDao::get_all();
        View::setTemplate("ludotheque.genre");
        View::bindVariable("genres", $genres);
        View::display();
    }

    public static function genre_addAction()
    {
        $libelleG = $_POST['libelleG'];

        if (!empty($libelleG)) {
            $libelleG = GenreDao::add($libelleG);
        } else {
            $router = new Router();
            $base_path = $router->getBasePath();
            header("Location:" . $base_path . "/admin/ludotheque?ajoutG=false");
        }
    }
}
