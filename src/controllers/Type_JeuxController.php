<?php

namespace Nathan\controllers;

use Nathan\templates\View;
use Nathan\controllers\Route;
use Nathan\controllers\Router;
use Nathan\dao\Type_JeuxDao;

class Type_JeuxController implements IController
{
    public static function route()
    {

        $router = new Router();
        $router->addRoute(new Route("/gaming/types_jeux", "Type_JeuxController", "types_jeuxAction"));
        $router->addRoute(new Route("/gaming/types_jeux/add", "Type_JeuxController", "types_jeux_addAction"));
        $route = $router->findRoute();

        if ($route) {
            $route->execute();
        } else {
            View::setTemplate("error404");
            View::display();
        }
    }

    public static function types_jeuxAction()
    {
        $types_jeux = Type_JeuxDao::get_all();
        View::setTemplate("gaming.type_jeux");
        View::bindVariable("types_jeux", $types_jeux);
        View::display();
    }
    public static function types_jeux_addAction()
    {
        $libelleTJ = $_POST['libelleTJ'];

        if (!empty($libelleTJ)) {
            $libelleTJ = Type_JeuxDao::add($libelleTJ);
        } else {
            $router = new Router();
            $base_path = $router->getBasePath();
            header("Location:" . $base_path . "/admin/gaming?ajoutTJ=false");
        }
    }
}
