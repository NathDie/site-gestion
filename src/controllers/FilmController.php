<?php

namespace Nathan\controllers;

use Nathan\templates\View;
use Nathan\controllers\Route;
use Nathan\controllers\Router;
use Nathan\dao\FilmDao;

class FilmController implements IController
{
    public static function route()
    {

        $router = new Router();
        $router->addRoute(new Route("/ludotheque/films", "FilmController", "filmAction"));
        $router->addRoute(new Route("/ludotheque/films/add", "FilmController", "film_addAction"));
        $route = $router->findRoute();

        if ($route) {
            $route->execute();
        } else {
            View::setTemplate("error404");
            View::display();
        }
    }

    public static function filmAction()
    {
        $films = FilmDao::get_all();
        View::setTemplate("ludotheque.film");
        View::bindVariable("films", $films);
        View::display();
    }

    public static function film_addAction()
    {
        $titreF = $_POST['titreF'];
        $anneeF = $_POST['dateF'];
        $auteurF = $_POST['auteurF'];
        $GenreF = $_POST['genreF'];
        $dd = $_POST['disque_dur'];

        if (!empty($titreF) && !empty($anneeF) && !empty($auteurF) && !empty($GenreF) && !empty($dd)) {
            $film = FilmDao::add($titreF, $anneeF, $auteurF, $GenreF, $dd);
        } else {
            $router = new Router();
            $base_path = $router->getBasePath();
            header("Location:" . $base_path . "/admin/ludotheque?ajoutF=false");
        }
    }
}
