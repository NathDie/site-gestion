<?php

namespace Nathan\controllers;

use Nathan\templates\View;
use Nathan\controllers\Route;
use Nathan\controllers\Router;
use Nathan\dao\JeuxDao;

class JeuxController implements IController
{
    public static function route()
    {

        $router = new Router();
        $router->addRoute(new Route("/gaming/jeux", "JeuxController", "jeuxAction"));
        $router->addRoute(new Route("/gaming/jeux/add", "JeuxController", "jeux_addAction"));
        $route = $router->findRoute();

        if ($route) {
            $route->execute();
        } else {
            View::setTemplate("error404");
            View::display();
        }
    }

    public static function jeuxAction()
    {
        $jeux = JeuxDao::get_all();
        View::setTemplate("gaming.jeux");
        View::bindVariable("jeux", $jeux);
        View::display();
    }

    public static function jeux_addAction()
    {
        $nomJ = $_POST['nomJ'];
        $dateJ = $_POST['date_sortie'];
        $platformeJ = $_POST['platformeJ'];
        $supportJ = $_POST['supportJ'];
        $Type_Jeux = $_POST['type_jeux'];

        if (!empty($nomJ) && !empty($dateJ) && !empty($platformeJ) && !empty($supportJ) && !empty($Type_Jeux)) {
            $jeux = JeuxDao::add($nomJ, $dateJ, $platformeJ, $supportJ, $Type_Jeux);
        } else {
            $router = new Router();
            $base_path = $router->getBasePath();
            header("Location:" . $base_path . "/admin/gaming?ajoutJ=false");
        }
    }
}
