<?php

namespace Nathan\controllers;

use Nathan\templates\View;
use Nathan\controllers\Route;
use Nathan\controllers\Router;
use Nathan\dao\AuteurDao;
use Nathan\dao\PlatformeDao;

class PlatformeController implements IController
{
    public static function route()
    {

        $router = new Router();
        $router->addRoute(new Route("/gaming/platformes", "PlatformeController", "platformeAction"));
        $router->addRoute(new Route("/gaming/platformes/add", "PlatformeController", "platforme_addAction"));
        $route = $router->findRoute();

        if ($route) {
            $route->execute();
        } else {
            View::setTemplate("error404");
            View::display();
        }
    }

    public static function platformeAction()
    {
        $platformes = PlatformeDao::get_all();
        View::setTemplate("gaming.platforme");
        View::bindVariable("platformes", $platformes);
        View::display();
    }

    public static function platforme_addAction()
    {
        $nomPJ = $_POST['nomPJ'];

        if (!empty($nomPJ)) {
            $nom = PlatformeDao::add($nomPJ);
        } else {
            $router = new Router();
            $base_path = $router->getBasePath();
            header("Location:" . $base_path . "/admin/gaming?ajoutPJ=false");
        }
    }
}
