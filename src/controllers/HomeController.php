<?php

namespace Nathan\controllers;

use Nathan\templates\View;
use Nathan\controllers\Route;
use Nathan\controllers\Router;

class HomeController implements IController
{
    public static function route()
    {

        $router = new Router();
        $router->addRoute(new Route("/", "HomeController", "homeAction"));
        $route = $router->findRoute();

        if ($route) {
            $route->execute();
        } else {
            View::setTemplate("error404");
            View::display();
        }
    }

    public static function homeAction()
    {
        View::setTemplate("index");
        View::display();
    }
}
