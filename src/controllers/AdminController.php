<?php

namespace Nathan\controllers;

use Nathan\templates\View;
use Nathan\controllers\Route;
use Nathan\controllers\Router;
use Nathan\dao\LoginDao;
use Nathan\dao\AuteurDao;
use Nathan\dao\GenreDao;
use Nathan\dao\PlatformeDao;
use Nathan\dao\RealisateurDao;
use Nathan\dao\SupportDao;
use Nathan\dao\Type_JeuxDao;
use Nathan\dao\TypeDao;

class AdminController implements IController
{
    public static function route()
    {

        $router = new Router();
        $router->addRoute(new Route("/admin/bibliotheque", "AdminController", "admin_bibliothequeAction"));
        $router->addRoute(new Route("/admin/ludotheque", "AdminController", "admin_ludothequeAction"));
        $router->addRoute(new Route("/admin/gaming", "AdminController", "admin_gamingAction"));
        $router->addRoute(new Route("/admin/leave", "AdminController", "leaveAction"));
        $router->addRoute(new Route("/login", "AdminController", "loginAction"));
        $route = $router->findRoute();

        if ($route) {
            $route->execute();
        } else {
            View::setTemplate("error404");
            View::display();
        }
    }

    public static function admin_bibliothequeAction()
    {
        session_start();
        if (!isset($_SESSION['username'])) {
            $router = new Router();
            $base_path = $router->getBasePath();

            header("Location: " . $base_path . "/");
            exit();
        } else {
            $types = TypeDao::get_libelle();
            $auteurs = AuteurDao::get_nom();
            View::bindVariable("auteurs", $auteurs);
            View::bindVariable("types", $types);
            View::setTemplate("admin.bibliotheque");
            View::display();
        }
    }

    public static function admin_ludothequeAction()
    {
        session_start();
        if (!isset($_SESSION['username'])) {
            $router = new Router();
            $base_path = $router->getBasePath();

            header("Location: " . $base_path . "/");
            exit();
        } else {
            $realisateurs = RealisateurDao::get_infos();
            $genres = GenreDao::get_libelle();
            View::bindVariable("realisateurs", $realisateurs);
            View::bindVariable("genres", $genres);
            View::setTemplate("admin.ludotheque");
            View::display();
        }
    }

    public static function admin_gamingAction()
    {
        session_start();
        if (!isset($_SESSION['username'])) {
            $router = new Router();
            $base_path = $router->getBasePath();

            header("Location: " . $base_path . "/");
            exit();
        } else {
            $types_jeux = Type_JeuxDao::get_libelle();
            $supportsJ = SupportDao::get_libelle();
            $platformesJ = PlatformeDao::get_nom();
            View::bindVariable("supportsJ", $supportsJ);
            View::bindVariable("types_jeux", $types_jeux);
            View::bindVariable("platformesJ", $platformesJ);
            View::setTemplate("admin.gaming");
            View::display();
        }
    }
    public static function leaveAction()
    {
        session_start();
        session_unset();
        session_destroy();
        $router = new Router();
        $base_path = $router->getBasePath();
        header("location:" . $base_path . "/");
    }

    public static function loginAction()
    {
        $username = $_POST['username'];
        $password = $_POST['password'];

        if (!empty($username) && !empty($password)) {
            $login = LoginDao::get($username, $password);
        } else {
            $router = new Router();
            $base_path = $router->getBasePath();
            header("Location:" . $base_path . "/");
        }
    }
}
