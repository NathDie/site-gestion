<?php

namespace Nathan;

use Nathan\templates\View;
use Nathan\controllers\Route;
use Nathan\controllers\Router;

require "vendor/autoload.php";



$router = new Router();
$router->addRoute(new Route("/", "HomeController"));
$router->addRoute(new Route("/error", "ErrorController"));
$router->addRoute(new Route("/bibliotheque", "BibliothequeController"));
$router->addRoute(new Route("/bibliotheque/livres", "LivreController"));
$router->addRoute(new Route("/bibliotheque/livres/{*}", "LivreController"));
$router->addRoute(new Route("/bibliotheque/auteurs", "AuteurController"));
$router->addRoute(new Route("/bibliotheque/auteurs/{*}", "AuteurController"));
$router->addRoute(new Route("/bibliotheque/types", "TypeController"));
$router->addRoute(new Route("/bibliotheque/types/{*}", "TypeController"));
$router->addRoute(new Route("/ludotheque", "LudothequeController"));
$router->addRoute(new Route("/ludotheque/films", "FilmController"));
$router->addRoute(new Route("/ludotheque/films/{*}", "FilmController"));
$router->addRoute(new Route("/ludotheque/realisateurs", "RealisateurController"));
$router->addRoute(new Route("/ludotheque/realisateurs/{*}", "RealisateurController"));
$router->addRoute(new Route("/ludotheque/genres", "GenreController"));
$router->addRoute(new Route("/ludotheque/genres/{*}", "GenreController"));
$router->addRoute(new Route("/gaming", "GamingController"));
$router->addRoute(new Route("/gaming/jeux", "JeuxController"));
$router->addRoute(new Route("/gaming/jeux/{*}", "JeuxController"));
$router->addRoute(new Route("/gaming/platformes", "PlatformeController"));
$router->addRoute(new Route("/gaming/platformes/{*}", "PlatformeController"));
$router->addRoute(new Route("/gaming/supports", "SupportController"));
$router->addRoute(new Route("/gaming/supports/{*}", "SupportController"));
$router->addRoute(new Route("/gaming/types_jeux", "Type_JeuxController"));
$router->addRoute(new Route("/gaming/types_jeux/{*}", "Type_JeuxController"));
$router->addRoute(new Route("/admin", "AdminController"));
$router->addRoute(new Route("/admin/{*}", "AdminController"));
$router->addRoute(new Route("/login", "AdminController"));
$route = $router->findRoute();

if ($route) {
    $route->execute();
} else {
    $router = new Router();
    $base_path = $router->getBasePath();
    header("Location:" . $base_path . "/error");
}
